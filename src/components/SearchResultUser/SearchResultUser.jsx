import React from 'react';
import { Link } from 'react-router-dom';
import './SearchResultUser.scss';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';
import { actionFollowRequest, actionUnFollowRequest } from '../../store/actions/followingAction';
import useShallowEqualSelector from '../../hooks/useShallowEqualSelector';
import { addAnotherUser } from '../../store/actions/userAction';

export const SearchResultUser = (props) => {
  const { user: currentUser } = useShallowEqualSelector((state) => state.user);
  const { firstName, lastName, username, avatarLink = '', userID, isAlreadyFollowing } = props;
  const hiddenBtn = currentUser.username === username;
  const dispatch = useDispatch();

  const avatar = `http://twitter-clone.loc:8091/${avatarLink}`;

  const followHandler = () => {
    const data = {
      followerID: currentUser.id,
      followingID: userID,
    };

    if (isAlreadyFollowing) {
      dispatch(actionUnFollowRequest(data));
    } else {
      dispatch(actionFollowRequest(data));
    }
  };

  const linkHandler = () => {
    if (!hiddenBtn) {
      const anotherUser = {
        id: userID,
        firstname: firstName,
        lastname: lastName,
        username,
        profileimg: avatarLink,
      };

      dispatch(addAnotherUser(anotherUser));
    }
  };

  return (
    <div className="search-result__user d-flex">
      <Link
        to={hiddenBtn ? '/profile' : `/user/${username}`}
        className="d-block mr-2"
        onClick={() => linkHandler()}
      >
        {avatarLink && <img src={avatar} alt="User avatar" className="search-result__photo" />}
        {!avatarLink && <span className="search-result__photo" />}
      </Link>

      <div className="search-result__info">
        <Link
          to={hiddenBtn ? '/profile' : `/user/${username}`}
          className="search-result__user-name"
          onClick={() => linkHandler()}
        >
          {firstName} {lastName}
        </Link>
        <Link
          to={hiddenBtn ? '/profile' : `/user/${username}`}
          className="search-result__nickname"
          onClick={() => linkHandler()}
        >
          @{username}
        </Link>
        {!isAlreadyFollowing && !hiddenBtn ? (
          <button className="following-card__btn" onClick={followHandler}>
            Following
          </button>
        ) : null}

        {isAlreadyFollowing && !hiddenBtn ? (
          <button
            className="following-card__btn  following-card__btn--unfollow"
            onClick={followHandler}
          >
            Unfollowing
          </button>
        ) : null}
      </div>
    </div>
  );
};

SearchResultUser.propTypes = {
  firstName: PropTypes.string,
  lastName: PropTypes.string,
  username: PropTypes.string,
  avatarLink: PropTypes.string,
  userID: PropTypes.string,
  isAlreadyFollowing: PropTypes.bool,
};
