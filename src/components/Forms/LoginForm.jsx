import React from 'react';
import { useForm } from 'react-hook-form';
import { Col, Container, Row } from 'react-bootstrap';
import { useDispatch } from 'react-redux';

import { actionUserLoginRequest } from '../../store/actions/userAction';
import useShallowEqualSelector from '../../hooks/useShallowEqualSelector';
import { mailRegex, usernameRegex, passwordRegex } from './validationRules';
import './Form.scss';

export function LoginForm() {
  const { register, handleSubmit, errors } = useForm();
  const dispatch = useDispatch();
  const { error } = useShallowEqualSelector((state) => state.user);
  const { isSignUpSuccess } = useShallowEqualSelector((state) => state.signUp);

  const submitDispatch = (data, event) => {
    event.preventDefault();

    dispatch(actionUserLoginRequest(data));
  };

  return (
    <div className="StartPage">
      <Container className="StartContainer">
        {isSignUpSuccess && (
          <div className="success-signup col-10 col-lg-5 mx-auto">
            <p>Your registration was successful.</p>
            <p>Enter your login and password to sign in.</p>
          </div>
        )}

        <form onSubmit={handleSubmit(submitDispatch)}>
          <Col xs={10} lg={5} className="Form StartForm mx-auto align-items-center">
            <Col className="px-0">
              <Col md={12}>
                <h1 className="Form_title pt-2">Log in to Devcommunication</h1>
              </Col>
            </Col>
            <Col lg={12} className="px-0">
              <Row className="mr-auto ml-3">
                <input
                  name="userLogin"
                  className="Form_input"
                  type="text"
                  placeholder="Email or Username"
                  ref={register({
                    // eslint-disable-next-line consistent-return
                    validate: (value) => {
                      const isEmailOrUsername = value.search('@');

                      if (isEmailOrUsername >= 0) {
                        return mailRegex.test(value) || 'Incorrect email or username';
                      }

                      if (isEmailOrUsername === -1) {
                        return usernameRegex.test(value) || 'Incorrect email or username';
                      }
                    },
                  })}
                />
              </Row>
              <Row className="ml-3">
                {/* eslint-disable-next-line no-nested-ternary */}
                {errors.userLogin ? (
                  <span className="error">{errors.userLogin.message}</span>
                ) : error ? (
                  <span className="error">{error}</span>
                ) : null}
              </Row>
            </Col>
            <Col lg={12} className="px-0">
              <Row className="mr-auto ml-3">
                <input
                  name="userPassword"
                  className="Form_input"
                  type="password"
                  placeholder="Password"
                  ref={register({
                    pattern: {
                      value: passwordRegex,
                      message: 'Incorrect password',
                    },
                  })}
                />
              </Row>
              <Row className="ml-3">
                {errors.userPassword && (
                  <span className="error">{errors.userPassword.message}</span>
                )}
              </Row>
            </Col>
            <Col className="px-0">
              <input className="Form_submit ml-3 my-3" type="submit" value="submit" />
            </Col>
          </Col>
        </form>
      </Container>
    </div>
  );
}
