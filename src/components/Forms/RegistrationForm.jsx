import React, { useState } from 'react';
import { Row, Col, Container } from 'react-bootstrap';
import DatePicker from 'react-datepicker';
import { useForm } from 'react-hook-form';
import { useHistory } from 'react-router';

import './Form.scss';
import 'react-datepicker/src/stylesheets/datepicker.scss';
import { useDispatch } from 'react-redux';
import { mailRegex, usernameRegex, passwordRegex, firstAndLastNameRegex } from './validationRules';
import useShallowEqualSelector from '../../hooks/useShallowEqualSelector';
import { actionSignUpRequest } from '../../store/actions/signUpAction';

export const RegistrationForm = () => {
  const [date, setDate] = useState(new Date());

  const DateHandler = (newDate) => {
    setDate(newDate);
  };

  const { register, handleSubmit, errors, getValues } = useForm();
  const history = useHistory();
  const dispatch = useDispatch();
  const { responseErrors, isSignUpSuccess } = useShallowEqualSelector((state) => state.signUp);

  const submitDispatch = (data, event) => {
    event.preventDefault();

    data.dateOfBirth = date;

    dispatch(actionSignUpRequest(data));
  };

  if (isSignUpSuccess) {
    history.push('/login');
  }

  return (
    <div className="StartPage">
      <Container className="StartContainer">
        <form onSubmit={handleSubmit(submitDispatch)}>
          <Col xs={10} lg={5} className="Form StartForm mx-auto align-items-center">
            <Col className="px-0">
              <Col md={12}>
                <h1 className="Form_title pt-2">Create account</h1>
              </Col>
            </Col>
            <Row lg={12}>
              <Col lg={6}>
                <Row className="mr-auto ml-3">
                  <input
                    name="firstName"
                    className="Form_input"
                    type="text"
                    placeholder="First name"
                    ref={register({
                      required: 'This is required.',
                      pattern: {
                        value: firstAndLastNameRegex,
                        message: 'Incorrect first name',
                      },
                    })}
                  />
                </Row>
                <Row className="ml-3">
                  {errors.firstName && <span className="error">{errors.firstName.message}</span>}
                </Row>
              </Col>
              <Col lg={6}>
                <Row className="mr-auto ml-3">
                  <input
                    name="lastName"
                    className="Form_input"
                    type="text"
                    placeholder="Last name"
                    ref={register({
                      required: 'This is required.',
                      pattern: {
                        value: firstAndLastNameRegex,
                        message: 'Incorrect last name',
                      },
                    })}
                  />
                </Row>
                <Row className="ml-3">
                  {errors.lastName && <span className="error">{errors.lastName.message}</span>}
                </Row>
              </Col>
            </Row>
            <Col lg={12} className="px-0">
              <Row className="mr-auto ml-3">
                <input
                  name="email"
                  className="Form_input"
                  type="email"
                  placeholder="Email"
                  ref={register({
                    required: 'This is required.',
                    pattern: {
                      value: mailRegex,
                      message: 'Incorrect email',
                    },
                  })}
                />
              </Row>
              <Row className="ml-3">
                {errors.email && <span className="error">{errors.email.message}</span>}
                {responseErrors.emailError && (
                  <span className="error">{responseErrors.emailError}</span>
                )}
              </Row>
            </Col>
            <Col lg={12} className="px-0">
              <Row className="mr-auto ml-3">
                <input
                  name="username"
                  className="Form_input"
                  type="text"
                  placeholder="Username"
                  ref={register({
                    required: 'This is required.',
                    pattern: {
                      value: usernameRegex,
                      message:
                        'Incorrect username. Username must be at least three(3) characters long and contain only letters and numbers',
                    },
                  })}
                />
              </Row>
              <Row className="ml-3">
                {errors.username && <span className="error">{errors.username.message}</span>}
                {responseErrors.usernameError && (
                  <span className="error">{responseErrors.usernameError}</span>
                )}
              </Row>
            </Col>
            <Col lg={12} className="px-0">
              <Row className="mr-auto ml-3">
                <DatePicker
                  className="Form_input"
                  placeholderText="Date of birth"
                  selected={date}
                  dateFormat="dd/MM/yyyy"
                  onChange={DateHandler}
                />
              </Row>
            </Col>
            <Col lg={12} className="px-0">
              <Row className="mr-auto ml-3">
                <input
                  name="userPassword"
                  className="Form_input"
                  type="password"
                  placeholder="Password"
                  ref={register({
                    required: 'This is required.',
                    pattern: {
                      value: passwordRegex,
                      message:
                        'Incorrect password. Password must be at least eight(8) character long and must\n' +
                        '\t\t\t\t\t\t\tcontain uppercase and number',
                    },
                  })}
                />
              </Row>
              <Row className="ml-3">
                {errors.userPassword && (
                  <span className="error">{errors.userPassword.message}</span>
                )}
              </Row>
            </Col>
            <Col lg={12} className="px-0">
              <Row className="mr-auto ml-3">
                <input
                  name="userConfPassword"
                  className="Form_input"
                  type="password"
                  placeholder="Confirm password"
                  ref={register({
                    validate: (value) => value === getValues('userPassword') || 'Password mismatch',
                  })}
                />
              </Row>
              <Row className="ml-3">
                {errors.userConfPassword && (
                  <span className="error">{errors.userConfPassword.message}</span>
                )}
              </Row>
            </Col>
            <Col className="px-0">
              <input className="Form_submit ml-3 my-3" type="submit" value="submit" />
            </Col>
          </Col>
        </form>
      </Container>
    </div>
  );
};
