import React, { useEffect, useState } from 'react';
import { Row, Col } from 'react-bootstrap';
import Alert from 'react-bootstrap/Alert';
import { useDispatch } from 'react-redux';
import { useForm } from 'react-hook-form';

import { MainButton } from '../MainButton/MainButton';
import './Form.scss';
import './EditProfileForm.scss';
import { passwordRegex } from './validationRules';
import useShallowEqualSelector from '../../hooks/useShallowEqualSelector';
import { actionChangePassRequest } from '../../store/actions/userAction';

export const ChangePassForm = () => {
  const { user, status, error, alert } = useShallowEqualSelector((state) => state.user);
  const { register, errors, watch, handleSubmit, formState, reset } = useForm();
  const { isSubmitted } = formState;
  const [isDisabled, setDisable] = useState(true);
  const fieldsWatcher = watch();
  const dispatch = useDispatch();

  useEffect(() => {
    if (fieldsWatcher.currentPassword && fieldsWatcher.newPassword && fieldsWatcher.confPassword) {
      if (fieldsWatcher.newPassword === fieldsWatcher.confPassword) {
        setDisable(false);
      } else {
        setDisable(true);
      }
    }
  }, [fieldsWatcher]);

  const sendNewPass = (data, event) => {
    event.preventDefault();

    data.userId = user.id;
    dispatch(actionChangePassRequest(data));
  };

  const alertHandler = () => {
    reset();
    setDisable(true);
  };

  return (
    <form onSubmit={handleSubmit(sendNewPass)}>
      <Col xs={10} lg={12} className="EditProfileForm mx-auto align-items-center mt-4 col-12">
        <Col className="px-0">
          <Col md={12}>
            <h1 className="Form_title pt-2">Change password</h1>
          </Col>
        </Col>
        <Col lg={12} className="px-0">
          <Row className="mr-auto ml-3">
            <input
              name="currentPassword"
              className="Form_input mr-0"
              type="password"
              placeholder="Current password"
              ref={register({
                required: 'This is required.',
                pattern: {
                  value: passwordRegex,
                  message: 'Invalid password',
                },
              })}
            />
          </Row>
          <Row className="ml-3">
            {errors.currentPassword && (
              <span className="error">{errors.currentPassword.message}</span>
            )}
          </Row>
        </Col>
        <Col lg={12} className="px-0">
          <Row className="mr-auto ml-3">
            <input
              name="newPassword"
              className="Form_input mr-0"
              type="password"
              placeholder="Password"
              ref={register({
                required: 'This is required.',
                pattern: {
                  value: passwordRegex,
                  message: 'Invalid password',
                  validate: (value) => value === '1' || 'error, should be 1',
                },
              })}
            />
          </Row>
          <Row className="ml-3">
            {errors.newPassword && <span className="error">{errors.newPassword.message}</span>}
          </Row>
        </Col>
        <Col lg={12} className="px-0">
          <Row className="mr-auto ml-3">
            <input
              name="confPassword"
              className="Form_input mr-0"
              type="password"
              placeholder="Confirm password"
              ref={register({
                required: 'This is required.',
                pattern: {
                  value: passwordRegex,
                  message: 'Invalid password',
                },
              })}
            />
          </Row>
          <Row className="ml-3">
            {errors.confPassword && <span className="error">{errors.confPassword.message}</span>}
          </Row>
        </Col>
        <Col lg={12} className="px-0">
          <Row className="flex-row-reverse ml-3 mr-1 my-3">
            <MainButton
              width={150}
              height={25}
              value="CHANGE PASSWORD"
              type="submit"
              disabled={isDisabled}
            />
          </Row>
          <Row>
            {isSubmitted && (status || error) ? (
              <Alert
                variant={alert}
                className="mx-3 Form_alert"
                onClose={() => alertHandler()}
                dismissible
              >
                <Alert.Heading>{status || error}</Alert.Heading>
              </Alert>
            ) : null}
          </Row>
        </Col>
      </Col>
    </form>
  );
};
