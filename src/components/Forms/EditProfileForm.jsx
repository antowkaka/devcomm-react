import React, { useState } from 'react';
import { Row, Col } from 'react-bootstrap';
import DatePicker from 'react-datepicker';
import { useDispatch } from 'react-redux';
import { useForm } from 'react-hook-form';

import Alert from 'react-bootstrap/Alert';
import { MainButton } from '../MainButton/MainButton';
import './Form.scss';
import './EditProfileForm.scss';
import 'react-datepicker/src/stylesheets/datepicker.scss';
import useShallowEqualSelector from '../../hooks/useShallowEqualSelector';
import { firstAndLastNameRegex, mailRegex, usernameRegex } from './validationRules';
import { formatDate } from '../../utils/dateFormatter';
import { actionUpdateInfoRequest } from '../../store/actions/userAction';

export const EditProfileForm = () => {
  const [count, setCount] = useState(0);
  const [isDisabled, setDisable] = useState(true);
  const { register, handleSubmit, errors, formState, reset } = useForm();
  const { dirty, isSubmitted } = formState;
  const { user, status, alert } = useShallowEqualSelector((state) => state.user);
  const dispatch = useDispatch();

  const checkFields = (val) => {
    if (!dirty || val === '') {
      setDisable(true);
    } else {
      setDisable(false);
    }
  };

  const [date, setDate] = useState(new Date(user.dob));

  const DateHandler = (newDate) => {
    if (formatDate(newDate) !== user.dob) {
      setDisable(false);
    } else {
      setDisable(true);
    }
    setDate(newDate);
  };

  const textareaHandler = (event) => {
    const textareaLength = event.target.value.length;

    checkFields(event.target.value);
    if (textareaLength < 200) {
      setCount(textareaLength);
    } else {
      setCount(200);
    }
  };

  const onSubmit = (data, e) => {
    e.preventDefault();

    const updUserData = {
      id: user.id,
      firstname: data.userName,
      lastname: data.userLastName,
      email: data.userEmail,
      username: data.username,
      profileimg: user.profileimg,
      dob: formatDate(date),
      bio: data.biography,
    };

    dispatch(actionUpdateInfoRequest(updUserData));
  };

  const alertHandler = () => {
    reset();
    setDisable(true);
    dispatch({ type: 'DELETE_STATUS', payload: '' });
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Col xs={10} lg={12} className="EditProfileForm mx-auto align-items-center col-12">
        <Col className="px-0">
          <Col md={12}>
            <h1 className="Form_title pt-2">Personal information</h1>
          </Col>
        </Col>
        <Row lg={12}>
          <Col lg={6}>
            <Row className="mr-auto ml-3">
              <input
                name="userName"
                className="Form_input mr-0"
                type="text"
                placeholder="First name"
                defaultValue={user.firstname}
                onChange={(e) => checkFields(e.target.value)}
                ref={register({
                  required: true,
                  pattern: {
                    value: firstAndLastNameRegex,
                    message: 'Incorrect first name',
                  },
                })}
              />
            </Row>
            <Row className="ml-3">
              {errors.userName && <span className="error">{errors.userName.message}</span>}
            </Row>
          </Col>
          <Col lg={6}>
            <Row className="mr-auto ml-3">
              <input
                name="userLastName"
                className="Form_input mr-0"
                type="text"
                placeholder="Last name"
                defaultValue={user.lastname}
                onChange={(e) => checkFields(e.target.value)}
                ref={register({
                  pattern: {
                    value: firstAndLastNameRegex,
                    message: 'Incorrect last name',
                  },
                })}
              />
            </Row>
            <Row className="ml-3">
              {errors.userLastName && <span className="error">{errors.userLastName.message}</span>}
            </Row>
          </Col>
        </Row>
        <Col lg={12} className="px-0">
          <Row className="mr-auto ml-3">
            <input
              name="userEmail"
              className="Form_input mr-0"
              type="email"
              placeholder="Email"
              defaultValue={user.email}
              onChange={(e) => checkFields(e.target.value)}
              ref={register({
                pattern: {
                  value: mailRegex,
                  message: 'Incorrect email',
                },
              })}
            />
          </Row>
          <Row className="ml-3">
            {errors.userEmail && <span className="error">{errors.userEmail.message}</span>}
          </Row>
        </Col>
        <Col lg={12} className="px-0">
          <Row className="mr-auto ml-3">
            <input
              name="username"
              className="Form_input mr-0"
              type="text"
              placeholder="Username"
              defaultValue={user.username}
              onChange={(e) => checkFields(e.target.value)}
              ref={register({
                pattern: {
                  value: usernameRegex,
                  message:
                    'Incorrect username. Username must be at least three(3) characters long and contain only letters and numbers',
                },
              })}
            />
          </Row>
          <Row className="ml-3">
            {errors.username && <span className="error">{errors.username.message}</span>}
          </Row>
        </Col>
        <Col xs={12} className="px-0">
          <Row className="mr-auto ml-3">
            <textarea
              className="Form_input Form_textarea col-12 mr-0"
              name="biography"
              rows="2"
              placeholder="About me"
              onChange={(e) => textareaHandler(e)}
              defaultValue={user.bio}
              ref={register({
                maxLength: {
                  value: 200,
                  message: 'Max length - 200',
                },
                minLength: {
                  value: 10,
                  message: 'Min length - 10',
                },
              })}
            />
          </Row>
          <Row className="flex-row-reverse ml-3 mr-1">
            <div className="text-right">{count}/1000</div>
          </Row>
          <Row className="ml-3">
            {errors.biography && <span className="error">{errors.biography.message}</span>}
          </Row>
        </Col>
        <Col lg={12} className="px-0">
          <Row className="mr-auto ml-3">
            <DatePicker
              name="dateBirth"
              className="Form_input"
              placeholderText="Date of birth"
              selected={date}
              dateFormat="dd/MM/yyyy"
              onChange={DateHandler}
            />
          </Row>
          <Row>
            <span className="error" />
          </Row>
        </Col>
        <Col lg={12} className="px-0">
          <Row className="flex-row-reverse ml-3 mr-1 my-3">
            <MainButton width={80} height={25} value="UPDATE" type="submit" disabled={isDisabled} />
          </Row>
          <Row>
            {isSubmitted && status && (
              <Alert
                variant={alert}
                className="mx-3 Form_alert"
                onClose={() => alertHandler()}
                dismissible
              >
                <Alert.Heading>{status}</Alert.Heading>
              </Alert>
            )}
          </Row>
        </Col>
      </Col>
    </form>
  );
};
