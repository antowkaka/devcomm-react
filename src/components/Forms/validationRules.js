export const mailRegex = new RegExp(
  /^([A-Za-z0-9][A-Za-z0-9]*\.?-?_?)*[A-Za-z0-9]*@([A-Za-z0-9]+([A-Za-z0-9-]*[A-Za-z0-9]+)*\.)+[A-Za-z]{2,}$/,
);
export const usernameRegex = new RegExp(/^[A-Za-z0-9]{3,}$/);
export const passwordRegex = new RegExp(/^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/);
export const firstAndLastNameRegex = new RegExp(/^[A-Za-zА-ЯЁа-яё]{3,}$/);
