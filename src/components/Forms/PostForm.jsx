import React, { useState, useRef } from 'react';
import { Col, Row, Alert } from 'react-bootstrap';
import { useDispatch } from 'react-redux';
import { PropTypes } from 'prop-types';
import { useForm } from 'react-hook-form';

import { MainButton } from '../MainButton/MainButton';
import './Form.scss';
import './PostForm.scss';
import { actionCreatePostRequest, actionGetAllPostsRequest } from '../../store/actions/postAction';
import useShallowEqualSelector from '../../hooks/useShallowEqualSelector';

export const PostForm = ({ userId }) => {
  const { register, handleSubmit, errors, watch, formState, reset } = useForm();
  const { isSubmitted } = formState;
  const dispatch = useDispatch();
  const fileInput = useRef(null);
  const [file, setFile] = useState(null);
  const { error } = useShallowEqualSelector((state) => state.post);

  const chooseFileHandler = () => {
    fileInput.current.click();
  };

  const handleFileChange = (e) => {
    setFile(e.target.files[0]);
  };

  const watchCount = watch('text', '').length;

  const createPost = (data, event) => {
    event.preventDefault();

    data.userId = userId;
    data.postImg = file;

    dispatch(actionCreatePostRequest(data));
    dispatch(actionGetAllPostsRequest());
  };

  const alertHandler = () => {
    reset();
    setFile(null);
  };

  return (
    <form onSubmit={handleSubmit(createPost)}>
      <Col xs={12} className="Form PostForm mb-3">
        <textarea
          className="Form_input Form_textarea col-12"
          name="text"
          rows="2"
          placeholder="What's happening?"
          ref={register({
            required: 'This is required.',
            maxLength: {
              value: 200,
              message: 'Max length - 200',
            },
            minLength: {
              value: 10,
              message: 'Min length - 10',
            },
          })}
        />
        <div className="text-right">{watchCount}/200</div>
        {errors.text && <span className="error">{errors.text.message}</span>}
        <Row className="flex-row-reverse pr-3 ">
          <MainButton value="POST" width={70} height={30} type="submit" />
          <MainButton
            classNames="mr-2"
            value="ADD FILE"
            width={100}
            height={30}
            callback={() => chooseFileHandler()}
          />
          <input
            className="d-none"
            type="file"
            ref={fileInput}
            onChange={(e) => handleFileChange(e)}
          />
        </Row>
        <Row>
          {/* eslint-disable-next-line no-nested-ternary */}
          {isSubmitted && errors.text ? (
            <Alert
              variant="danger"
              className="mx-3 my-2 Form_alert PostForm_alert"
              onClose={() => alertHandler()}
              dismissible
            >
              <Alert.Heading>{errors.text.message}</Alert.Heading>
            </Alert>
          ) : isSubmitted && error ? (
            <Alert
              variant="danger"
              className="mx-3 my-2 Form_alert PostForm_alert"
              onClose={() => alertHandler()}
              dismissible
            >
              <Alert.Heading>{error}</Alert.Heading>
            </Alert>
          ) : null}
        </Row>
      </Col>
    </form>
  );
};

PostForm.propTypes = {
  userId: PropTypes.string,
};
