import React from 'react';

import './RecommendsBlock.scss';
import { RecommendUser } from '../RecommendUser/RecommendUser';
import useShallowEqualSelector from '../../hooks/useShallowEqualSelector';

export const RecommendsBlock = () => {
  const { user: currentUser } = useShallowEqualSelector((state) => state.user);
  const { following } = useShallowEqualSelector((state) => state.following);
  const { whoToFollow } = useShallowEqualSelector((state) => state.allUsers);

  const recommendsUsers = whoToFollow.filter((user) => user.id !== currentUser.id).slice(0, 3);

  return (
    <div className="right-sidebar__follow col-12 p-3">
      <span className="d-inline m-0 right-sidebar__title">Who to follow</span>
      &middot;
      <a href="#" className="m-0 right-sidebar__link">
        View all
      </a>
      {whoToFollow &&
        recommendsUsers.map((user) => {
          const results = following ? following.filter((item) => item.id === user.id) : [];
          const isAlreadyFollowing = results.length > 0;

          return (
            <RecommendUser
              key={user.id}
              firstName={user.firstname}
              lastName={user.lastname}
              username={user.username}
              avatarLink={user.profileimg}
              userID={user.id}
              isAlreadyFollowing={isAlreadyFollowing}
            />
          );
        })}
    </div>
  );
};
