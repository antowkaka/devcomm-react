import React from 'react';
import './CopyrightBlock.scss';
import { Col, Row } from 'react-bootstrap';

export const CopyrightBlock = () => {
  return (
    <Col className="card mt-3 copyright">
      <Row className="list-group list-group-flush">
        <Row className="mx-3 list-group-item copyright__year">&copy; 2020 Devcommunication</Row>
        <Row className="mx-3 list-group-item copyright__link">
          <a href="#">Advertise with Devcommunication</a>
        </Row>
      </Row>
    </Col>
  );
};
