import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import './FollowCard.scss';
import { useDispatch } from 'react-redux';
import useShallowEqualSelector from '../../hooks/useShallowEqualSelector';
import { actionFollowRequest, actionUnFollowRequest } from '../../store/actions/followingAction';

export const FollowCard = (props) => {
  const { user: currentUser } = useShallowEqualSelector((state) => state.user);
  const dispatch = useDispatch();

  const {
    firstName,
    lastName,
    username,
    avatarLink = '',
    userID,
    isAlreadyFollowing,
    isFollower = '',
    hiddenBtn = false,
  } = props;

  const avatar = `http://twitter-clone.loc:8091/${avatarLink}`;

  const followHandler = () => {
    const data = {
      followerID: currentUser.id,
      followingID: userID,
    };

    if (isAlreadyFollowing) {
      dispatch(actionUnFollowRequest(data));
    } else {
      dispatch(actionFollowRequest(data));
    }
  };

  return (
    <div className="card following-card p-0">
      <div className="following-card__bg" />
      <div className="following-card-wrapper">
        <Link to="/profile">
          {avatarLink && <img src={avatar} alt="User avatar" className="following-card__photo" />}
          {!avatarLink && <div className="following-card__photo" />}
        </Link>
        <div className="following-card__info">
          <Link to="/profile" className="following-card__name">
            {firstName} {lastName}
          </Link>
          <div>
            <Link to="/profile" className="following-card__nickname">
              @{username}
            </Link>

            {isFollower && <span className="following-card__your">{isFollower}</span>}
          </div>
          {!isAlreadyFollowing && !hiddenBtn ? (
            <button className="following-card__btn" onClick={followHandler}>
              Following
            </button>
          ) : null}

          {isAlreadyFollowing && !hiddenBtn ? (
            <button
              className="following-card__btn  following-card__btn--unfollow"
              onClick={followHandler}
            >
              Unfollowing
            </button>
          ) : null}
        </div>
      </div>
    </div>
  );
};

FollowCard.propTypes = {
  firstName: PropTypes.string,
  lastName: PropTypes.string,
  username: PropTypes.string,
  avatarLink: PropTypes.string,
  userID: PropTypes.string,
  isAlreadyFollowing: PropTypes.bool,
  isFollower: PropTypes.string,
  hiddenBtn: PropTypes.bool,
};
