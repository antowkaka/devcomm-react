import React, { useState } from 'react';
import { Col, Row } from 'react-bootstrap';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHeart, faEllipsisH } from '@fortawesome/free-solid-svg-icons';
import './PostComponent.scss';
import { useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import { PostModalComponent } from './PostModalComponent';
import useShallowEqualSelector from '../../hooks/useShallowEqualSelector';
import { actionDislikePost, actionLikePost } from '../../store/actions/postAction';
import { addAnotherUser } from '../../store/actions/userAction';

export const PostComponent = (props) => {
  const {
    id,
    userId,
    firstName,
    lastName,
    username,
    avatarLink,
    date,
    text,
    imgLink = '',
    isLike = false,
    likes,
  } = props;

  const [state, setState] = useState({
    like: {
      isLike,
      likes,
    },
  });

  const postImg = `http://twitter-clone.loc:8091/${imgLink}`;
  const dispatch = useDispatch();

  const [modalShow, setModalShow] = React.useState(false);

  const { user } = useShallowEqualSelector((state) => state.user);

  const optionsHandler = () => {
    if (username === user.username) {
      setModalShow(true);
    }
  };

  const avatar = `http://twitter-clone.loc:8091/${avatarLink}`;
  const myPost = username === user.username;

  const likeClickHandler = () => {
    if (!state.like.isLike) {
      setState({ like: { isLike: true, likes: state.like.likes + 1 } });
      dispatch(actionLikePost({ postId: id, userId: user.id }));
    } else {
      setState({ like: { isLike: false, likes: state.like.likes - 1 } });
      dispatch(actionDislikePost({ postId: id, userId: user.id }));
    }
  };

  const linkHandler = () => {
    if (!myPost) {
      const anotherUser = {
        id: userId,
        firstname: firstName,
        lastname: lastName,
        username,
        profileimg: avatarLink,
      };

      dispatch(addAnotherUser(anotherUser));
    }
  };

  return (
    <Col md={12} className="Post mb-2 pl-0 pl-sm-3 pr-3 ">
      <div className="Post_user d-flex">
        <Col xs={2} className="px-0">
          {avatarLink ? (
            <img src={avatar} alt="User avatar" className="Post_user_avatar" />
          ) : (
            <div className="Post_user_avatar" />
          )}
        </Col>
        <Col>
          <Row>
            <Link
              to={myPost ? '/profile' : `/user/${username}`}
              className="Post_user_link Post_user_link_name"
              onClick={() => linkHandler()}
            >
              {firstName} {lastName}
            </Link>
            <Link
              to={myPost ? '/profile' : `/user/${username}`}
              className="Post_user_link"
              onClick={() => linkHandler()}
            >
              @{username}
            </Link>
            &middot;
            <Link
              to={myPost ? '/profile' : `/user/${username}`}
              className="Post_user_link"
              onClick={() => linkHandler()}
            >
              {date}
            </Link>
            {myPost && (
              <FontAwesomeIcon
                className="Post_opt ml-auto"
                icon={faEllipsisH}
                size="lg"
                onClick={() => optionsHandler()}
                cursor="pointer"
              />
            )}
            <br />
          </Row>
          <Row>
            <p className="Post_user_text my-2">{text}</p>
            {imgLink && (
              // eslint-disable-next-line jsx-a11y/img-redundant-alt
              <img className="Post_user_image img-fluid" src={postImg} alt="Post image" />
            )}
          </Row>
          <Row className="align-items-center">
            <FontAwesomeIcon
              className="Post_user_like"
              icon={faHeart}
              size="lg"
              color={state.like.isLike ? 'red' : '#8899a6'}
              onClick={likeClickHandler}
              cursor="pointer"
            />
            <span
              className="Post_user_like Post_user_like_quantity"
              style={{ color: state.like.isLike ? 'red' : '#8899a6' }}
            >
              {state.like.likes}
            </span>
          </Row>
        </Col>
      </div>

      <PostModalComponent
        modalId={id}
        firstName={firstName}
        lastName={lastName}
        username={username}
        avatarLink={avatarLink}
        avatar={avatar}
        date={date}
        text={text}
        show={modalShow}
        onHide={() => setModalShow(false)}
      />
    </Col>
  );
};

PostComponent.propTypes = {
  id: PropTypes.string,
  userId: PropTypes.string,
  firstName: PropTypes.string,
  lastName: PropTypes.string,
  username: PropTypes.string,
  avatarLink: PropTypes.string,
  date: PropTypes.string,
  text: PropTypes.string,
  imgLink: PropTypes.string,
  isLike: PropTypes.bool,
  likes: PropTypes.number,
};
