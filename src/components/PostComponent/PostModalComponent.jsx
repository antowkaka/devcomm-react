import React, { useState } from 'react';
import { Modal, Col, Row, Alert } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimes } from '@fortawesome/free-solid-svg-icons';
import PropTypes from 'prop-types';
import { useForm } from 'react-hook-form';

import { useDispatch } from 'react-redux';
import { MainButton } from '../MainButton/MainButton';
import {
  actionDeletePostRequest,
  actionGetAllPostsRequest,
  actionUpdatePostRequest,
} from '../../store/actions/postAction';
import useShallowEqualSelector from '../../hooks/useShallowEqualSelector';

export const PostModalComponent = (props) => {
  const {
    modalId,
    firstName,
    lastName,
    username,
    avatar,
    avatarLink,
    date,
    text,
    onHide,
    show,
  } = props;

  const [isDisabled, setDisable] = useState(true);
  const { register, errors, handleSubmit, formState } = useForm();
  const { error } = useShallowEqualSelector((state) => state.post);
  const { user } = useShallowEqualSelector((state) => state.user);
  const dispatch = useDispatch();
  const { dirty } = formState;

  const checkFields = (val) => {
    if (!dirty || val === '') {
      setDisable(true);
    } else {
      setDisable(false);
    }
  };

  const deletePost = (event) => {
    event.preventDefault();

    dispatch(actionDeletePostRequest(modalId));
    dispatch(actionGetAllPostsRequest(user.id));
  };

  const updatePost = (data, event) => {
    event.preventDefault();

    data.postId = modalId;

    dispatch(actionUpdatePostRequest(data));
  };

  return (
    <Modal
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
      show={show}
      onHide={onHide}
    >
      <form onSubmit={handleSubmit(updatePost)} className="col-12 p-0">
        <Col md={12} className="Post pr-3">
          <div className="Post_user d-flex">
            <Col xs={2} className="px-0">
              {avatarLink ? (
                <img src={avatar} alt="User avatar" className="Post_user_avatar" />
              ) : (
                <div className="Post_user_avatar" />
              )}
            </Col>
            <Col>
              <Row>
                <a href="#" className="Post_user_link Post_user_link_name">
                  {firstName} {lastName}
                </a>
                <a href="#" className="Post_user_link">
                  @{username}
                </a>
                &middot;
                <a href="#" className="Post_user_link">
                  {date}
                </a>
                <FontAwesomeIcon
                  className="ml-auto Post_close-modal"
                  icon={faTimes}
                  size="lg"
                  onClick={onHide}
                  cursor="pointer"
                />
                <br />
              </Row>
              <Row>
                <textarea
                  className="Form_input Form_textarea col-12"
                  name="postText"
                  rows="2"
                  defaultValue={text}
                  onChange={(e) => checkFields(e.target.value)}
                  ref={register({
                    required: 'This is required.',
                    maxLength: {
                      value: 200,
                      message: 'Max length - 200',
                    },
                    minLength: {
                      value: 10,
                      message: 'Min length - 10',
                    },
                  })}
                />
              </Row>
            </Col>
          </div>
        </Col>
        <Modal.Footer>
          <MainButton value="DELETE" width={70} height={30} callback={(e) => deletePost(e)} />
          <MainButton value="UPDATE" width={70} height={30} type="submit" disabled={isDisabled} />
        </Modal.Footer>
        {/* eslint-disable-next-line no-nested-ternary */}
        {errors.postText ? (
          <Alert variant="danger" className="mx-3">
            {errors.postText.message}
          </Alert>
        ) : error ? (
          <Alert variant="danger" className="mx-3">
            {error}
          </Alert>
        ) : null}
      </form>
    </Modal>
  );
};

PostModalComponent.propTypes = {
  modalId: PropTypes.string,
  firstName: PropTypes.string,
  lastName: PropTypes.string,
  username: PropTypes.string,
  avatarLink: PropTypes.string,
  date: PropTypes.string,
  text: PropTypes.string,
  avatar: PropTypes.string,
  onHide: PropTypes.func,
  show: PropTypes.bool,
};
