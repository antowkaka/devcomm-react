import React from 'react';
import './RecommendUser.scss';
import { Col, Row } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimes } from '@fortawesome/free-solid-svg-icons';
import { PropTypes } from 'prop-types';
import { useDispatch } from 'react-redux';
import useShallowEqualSelector from '../../hooks/useShallowEqualSelector';
import { actionFollowRequest, actionUnFollowRequest } from '../../store/actions/followingAction';
import { actionDeleteFromWhoToFollow } from '../../store/actions/allUsersAction';

export const RecommendUser = (props) => {
  const { user: currentUser } = useShallowEqualSelector((state) => state.user);
  const { whoToFollow } = useShallowEqualSelector((state) => state.allUsers);

  const dispatch = useDispatch();

  const { firstName, lastName, username, avatarLink = null, userID, isAlreadyFollowing } = props;

  const avatar = `http://twitter-clone.loc:8091/${avatarLink}`;

  const followHandler = () => {
    const data = {
      followerID: currentUser.id,
      followingID: userID,
    };

    if (isAlreadyFollowing) {
      dispatch(actionUnFollowRequest(data));
    } else {
      dispatch(actionFollowRequest(data));
    }
  };

  const deleteFromRecommendation = () => {
    const data = whoToFollow.filter((user) => user.id !== userID);

    dispatch(actionDeleteFromWhoToFollow(data));
  };

  return (
    <Row className="RecommendUser p-0 m-0">
      <Col xs={3} className="p-0 d-flex align-items-center">
        <a href="./profile" className="d-block">
          {avatarLink && <img src={avatar} alt="User avatar" className="RecommendUser_avatar" />}
          {!avatarLink && <span className="RecommendUser_avatar" />}
        </a>
      </Col>
      <Col xs={7} className="RecommendUser_info px-2 d-flex align-items-center">
        <Row className="ml-2 flex-column">
          <Row className="px-3">
            <a href="#" className="RecommendUser_info_name">
              {firstName} {lastName}
            </a>
          </Row>
          <Row className="px-3">
            <a href="#" className="RecommendUser_info_username">
              @{username}
            </a>
          </Row>
          <Row className="px-3">
            {!isAlreadyFollowing && (
              <button className="RecommendUser-card__btn" onClick={followHandler}>
                Following
              </button>
            )}

            {isAlreadyFollowing && (
              <button
                className="RecommendUser-card__btn RecommendUser-card__btn--unfollow"
                onClick={followHandler}
              >
                Unfollowing
              </button>
            )}
          </Row>
        </Row>
      </Col>
      <Col xs={2} className="d-flex align-items-center">
        <FontAwesomeIcon
          icon={faTimes}
          size="1x"
          className="RecommendUser_close"
          onClick={deleteFromRecommendation}
        />
      </Col>
    </Row>
  );
};

RecommendUser.propTypes = {
  firstName: PropTypes.string,
  lastName: PropTypes.string,
  username: PropTypes.string,
  avatarLink: PropTypes.string,
  userID: PropTypes.string,
  isAlreadyFollowing: PropTypes.bool,
};
