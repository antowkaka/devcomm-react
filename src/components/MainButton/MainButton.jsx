import React from 'react';
import { PropTypes } from 'prop-types';
import './MainButton.scss';

export const MainButton = (props) => {
  const {
    classNames = '',
    value,
    callback = null,
    width,
    height,
    disabled = false,
    type = 'button',
  } = props;
  let style = 'MainButton';
  if (classNames !== '') {
    style += ` ${classNames}`;
  }
  return (
    <input
      className={style}
      name={value}
      value={value}
      type={type}
      onClick={callback}
      style={{ width: `${width}px`, height: `${height}px` }}
      disabled={disabled}
    />
  );
};

MainButton.propTypes = {
  classNames: PropTypes.string,
  value: PropTypes.string,
  width: PropTypes.number,
  height: PropTypes.number,
  callback: PropTypes.func,
  disabled: PropTypes.bool,
  type: PropTypes.string,
};
