import React from 'react';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import { useHistory } from 'react-router';
import { useForm } from 'react-hook-form';
import { Nav, Navbar, Form, FormControl } from 'react-bootstrap';

import { MainButton } from '../MainButton/MainButton';
import './Header.scss';
import { actionUserLogoutRequest } from '../../store/actions/userAction';
import { actionSearchRequest } from '../../store/actions/searchAction';
import useShallowEqualSelector from '../../hooks/useShallowEqualSelector';

export const Header = (props = '') => {
  const { newClass } = props;
  const { register, handleSubmit } = useForm();
  const dispatch = useDispatch();
  const history = useHistory();
  const { hasAuthenticated } = useShallowEqualSelector((state) => state.user);
  const headerClasses = ['container-fluid p-0 header-home'];

  const submitDispatch = (data, event) => {
    event.preventDefault();

    dispatch(actionSearchRequest(data));

    history.push('/search');
  };

  if (newClass !== '') {
    headerClasses.push(newClass);
  }

  return (
    <header className={headerClasses.join(' ')}>
      <div className="container-fluid fixed-top header__nav">
        <div className="container p-0">
          <Navbar expand="md">
            <Navbar.Brand href="#home" className="d-md-none">
              <img
                src="../../public/img/icons/dev-comm-logo.svg"
                alt="Home"
                width="22"
                height="19"
              />
            </Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
              <Nav className="navbar-nav font-weight-bold d-flex align-items-center">
                <li className="nav-item mr-2">
                  <Link className="nav-link d-flex header__home" to="/home">
                    <img
                      src="../../public/img/icons/home-icon.svg"
                      alt="Home"
                      width="22"
                      height="19"
                    />
                    <span className="ml-1">Home</span>
                  </Link>
                </li>
              </Nav>

              <Link className="m-auto header__logo" to="/home">
                <img
                  src="../../public/img/icons/dev-comm-logo.svg"
                  alt="Devcomm logo"
                  width="20"
                  height="20"
                />
              </Link>

              <Form
                inline
                className="my-2 my-lg-0 mr-3 ml-md-auto header__form"
                onSubmit={handleSubmit(submitDispatch)}
              >
                <FormControl
                  type="text"
                  placeholder="Search"
                  className="form-control mr-sm-2 header__search"
                  aria-label="Search"
                  name="searchQuery"
                  ref={register({
                    validate: (value) => value !== '',
                  })}
                />
              </Form>

              <div className="nav-link-wrapper">
                <MainButton value="Chat" width={80} height={33} disabled />
                {hasAuthenticated ? (
                  <Link
                    className="nav-link m-auto header__logout"
                    to="/"
                    onClick={() => dispatch(actionUserLogoutRequest())}
                  >
                    Logout
                  </Link>
                ) : (
                  <Link className="nav-link m-auto header__logout" to="/">
                    Login
                  </Link>
                )}
              </div>
            </Navbar.Collapse>
          </Navbar>
        </div>
      </div>
    </header>
  );
};

Header.propTypes = {
  newClass: PropTypes.string,
};
