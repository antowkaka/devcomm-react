import React from 'react';
import './UserCard.scss';
import { Link } from 'react-router-dom';
import { Col, Row } from 'react-bootstrap';
import PropTypes from 'prop-types';

export const UserCard = (props) => {
  const {
    firstName,
    lastName,
    username,
    postCount = 0,
    followingCount = 0,
    followersCount = 0,
    avatarLink = '',
  } = props;

  const avatar = `http://twitter-clone.loc:8091/${avatarLink}`;

  return (
    <div className="UserCard">
      <div className="UserCard_bg" />
      <div className="UserCard_wrapper">
        <Link to="/profile">
          {avatarLink ? (
            <img src={avatar} alt="User avatar" className="UserCard_wrapper_avatar" />
          ) : (
            <div className="UserCard_wrapper_avatar" />
          )}
        </Link>

        <div className="UserCard_wrapper_info">
          <Link to="/profile" className="UserCard_wrapper_info_name">
            {firstName} {lastName}
          </Link>
          <Link to="/profile" className="UserCard_wrapper_info_nickname">
            @{username}
          </Link>
        </div>
      </div>

      <Row className="UserCard_group">
        <Col className="UserCard_group_info">
          <Row className="UserCard_group_info_title">Posts</Row>
          <Row className="UserCard_group_info_quantity">{postCount}</Row>
        </Col>

        <Col className="UserCard_group_info">
          <Row className="UserCard_group_info_title">Following</Row>
          <Row className="UserCard_group_info_quantity">{followingCount}</Row>
        </Col>

        <Col className="UserCard_group_info mr-0">
          <Row className="UserCard_group_info_title">Followers</Row>
          <Row className="UserCard_group_info_quantity">{followersCount}</Row>
        </Col>
      </Row>
    </div>
  );
};

UserCard.propTypes = {
  firstName: PropTypes.string,
  lastName: PropTypes.string,
  username: PropTypes.string,
  avatarLink: PropTypes.string,
  postCount: PropTypes.number,
  followingCount: PropTypes.number,
  followersCount: PropTypes.number,
};
