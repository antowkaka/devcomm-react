import React from 'react';
import { NavLink } from 'react-router-dom';
import PropTypes from 'prop-types';

export const StartButton = (props) => {
  const { text, path, callback = null } = props;

  return (
    <NavLink
      to={path}
      className="StartPage_login"
      /* eslint-disable-next-line react/jsx-no-bind */
      onClick={callback}
    >
      <svg width="277" height="62">
        <defs>
          <linearGradient id="grad2">
            <stop offset="0%" stopColor="#FF8282" />
            <stop offset="100%" stopColor="#E178ED" />
          </linearGradient>
        </defs>
        <rect x="5" y="5" rx="25" fill="none" stroke="url(#grad2)" width="266" height="50" />
      </svg>
      <span>{text}</span>
    </NavLink>
  );
};

StartButton.propTypes = {
  text: PropTypes.string,
  path: PropTypes.string,
  callback: PropTypes.func,
};
