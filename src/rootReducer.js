import { combineReducers } from 'redux';

import { postReducer } from './store/reducers/postReducer';
import { userReducer } from './store/reducers/userReducer';
import { signUpReducer } from './store/reducers/signUpReducer';
import { searchReducer } from './store/reducers/searchReducer';
import { allUsersReducer } from './store/reducers/allUsersReducer';
import { followersReducer } from './store/reducers/followersReducer';
import { followingReducer } from './store/reducers/followingReducer';

export default combineReducers({
  post: postReducer,
  user: userReducer,
  signUp: signUpReducer,
  search: searchReducer,
  allUsers: allUsersReducer,
  followers: followersReducer,
  following: followingReducer,
});
