import { all } from 'redux-saga/effects';
import { postWatcher } from '../sagas/postSaga';
import { userWatcher } from '../sagas/userSaga';
import { signUpWatcher } from '../sagas/signUpSaga';
import { authWatcher } from '../sagas/authSaga';
import { searchWatcher } from '../sagas/searchSaga';
import { allUsersWatcher } from '../sagas/allUsersSaga';
import { followersWatcher } from '../sagas/followersSaga';
import { followingWatcher } from '../sagas/followingSaga';

export function* rootSaga() {
  yield all([
    postWatcher(),
    userWatcher(),
    signUpWatcher(),
    authWatcher(),
    searchWatcher(),
    allUsersWatcher(),
    followersWatcher(),
    followingWatcher(),
  ]);
}
