import { put, takeLatest } from 'redux-saga/effects';

import { FOLLOW_REQUEST, GET_FOLLOWING_REQUEST, UNFOLLOW_REQUEST } from '../actionTypes';
import {
  actionFollowFailed,
  actionFollowSuccess,
  actionGetFollowingFailed,
  actionGetFollowingSuccess,
  actionUnFollowFailed,
  actionUnFollowSuccess,
} from '../actions/followingAction';

function* getFollowing(action) {
  const { payload } = action;

  const result = yield fetch(
    `http://twitter-clone.loc:8091/api/user/get-all-following/${payload.data}`,
  )
    .then((response) => response.json())
    .then((resolve) => {
      if (resolve.errors) {
        return {
          responseErrors: resolve.errors,
        };
      }
      return {
        following: resolve.following,
        responseErrors: '',
      };
    })
    .catch((reject) => {
      return {
        responseErrors: reject.message,
      };
    });

  if (result.responseErrors) {
    yield put(actionGetFollowingFailed(result.responseErrors));
  } else {
    yield put(actionGetFollowingSuccess(result.following));
  }
}

function* follow(action) {
  const { payload } = action;

  const result = yield fetch('http://twitter-clone.loc:8091/api/user/follow', {
    method: 'POST',
    mode: 'cors',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(payload.data),
  })
    .then((response) => response.json())
    .then((resolve) => {
      if (resolve.error) {
        return {
          responseErrors: resolve.error,
        };
      }
      return {
        following: resolve.following,
        responseErrors: '',
      };
    })
    .catch((reject) => {
      return {
        isSignUpSuccess: false,
        responseErrors: reject.message,
      };
    });

  if (result.responseErrors) {
    yield put(actionFollowFailed(result.responseErrors));
  } else {
    yield put(actionFollowSuccess(result.following));
  }
}

function* unfollow(action) {
  const { payload } = action;

  const result = yield fetch('http://twitter-clone.loc:8091/api/user/unfollow', {
    method: 'POST',
    mode: 'cors',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(payload.data),
  })
    .then((response) => response.json())
    .then((resolve) => {
      if (resolve.error) {
        return {
          responseErrors: resolve.error,
        };
      }
      return {
        following: resolve.following,
        responseErrors: '',
      };
    })
    .catch((reject) => {
      return {
        isSignUpSuccess: false,
        responseErrors: reject.message,
      };
    });

  if (result.responseErrors) {
    yield put(actionUnFollowFailed(result.responseErrors));
  } else {
    yield put(actionUnFollowSuccess(result.following));
  }
}

export function* followingWatcher() {
  yield takeLatest(GET_FOLLOWING_REQUEST, getFollowing);
  yield takeLatest(FOLLOW_REQUEST, follow);
  yield takeLatest(UNFOLLOW_REQUEST, unfollow);
}
