import { takeLatest, put } from 'redux-saga/effects';

import { CHECK_AUTH } from '../actionTypes';
import { getToken } from '../../utils/authToken';
import { checkAuthSuccess, checkAuthFailed } from '../actions/authAction';

function* sendToken() {
  const token = getToken();

  const result = yield fetch('http://twitter-clone.loc:8091/api/user/auth-check', {
    method: 'POST',
    mode: 'cors',
    headers: {
      'Content-Type': 'application/json',
    },
    body: token,
  })
    .then((res) => res.json())
    .then((resolve) => {
      if (resolve.status === 'Failed') {
        return {
          status: false,
          error: resolve.error,
        };
      }
      return {
        user: resolve.user,
        status: true,
        error: '',
      };
    })
    .catch((reject) => {
      return {
        error: reject.message,
      };
    });

  if (result.error) {
    yield put(checkAuthFailed(result.error));
  } else {
    yield put(checkAuthSuccess(result));
  }
}

export function* authWatcher() {
  yield takeLatest(CHECK_AUTH, sendToken);
}
