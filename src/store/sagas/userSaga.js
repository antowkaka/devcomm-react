import { takeLatest, put } from 'redux-saga/effects';
import {
  USER_LOGIN_REQUEST,
  USER_LOGOUT_REQUEST,
  USER_UPDATE_AVATAR_REQUEST,
  USER_UPDATE_INFO_REQUEST,
  USER_CHANGE_PASS_REQUEST,
} from '../actionTypes';
import {
  actionUserLoginSuccess,
  actionUserLoginFailed,
  actionUserLogoutSuccess,
  actionUserLogoutFailed,
  actionUpdateAvatarSuccess,
  actionUpdateAvatarFailed,
  actionUpdateInfoSuccess,
  actionUpdateInfoFailed,
  actionChangePassSuccess,
  actionChangePassFailed,
} from '../actions/userAction';
import { removeToken, setToken } from '../../utils/authToken';

function* userLogin(action) {
  const { payload } = action;

  const result = yield fetch('http://twitter-clone.loc:8091/api/user/login', {
    method: 'POST',
    mode: 'cors',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(payload.data),
  })
    .then((response) => response.json())
    .then((resolve) => {
      if (resolve.error) {
        return {
          user: {},
          error: resolve.error,
        };
      }

      setToken(resolve.token);
      delete resolve.token;

      return {
        user: resolve,
        error: '',
      };
    })
    .catch((reject) => {
      return {
        user: {},
        error: reject.message,
      };
    });

  if (result.error) {
    yield put(actionUserLoginFailed(result.error));
  } else {
    yield put(actionUserLoginSuccess(result.user));
  }
}

function* userLogout() {
  removeToken();

  const result = yield fetch('http://twitter-clone.loc:8091/api/user/logout')
    .then((response) => response.json())
    .then((resolve) => resolve.status)
    .catch((reject) => {
      return {
        error: reject.message,
      };
    });

  if (result.error) {
    yield put(actionUserLogoutFailed(result.error));
  } else {
    yield put(actionUserLogoutSuccess(result));
  }
}

function* updateAvatar(action) {
  const { payload } = action;
  const formData = new FormData();

  formData.append('avatar', payload.data.avatar[0]);
  formData.append('userId', payload.data.userId);

  const result = yield fetch('http://twitter-clone.loc:8091/api/user/update-avatar', {
    method: 'POST',
    body: formData,
  })
    .then((response) => response.json())
    .then((resolve) => {
      if (resolve.error) {
        return {
          user: {},
          error: resolve.error,
        };
      }
      return {
        user: resolve,
        error: '',
      };
    })
    .catch((reject) => {
      return {
        user: {},
        error: reject.message,
      };
    });

  if (result.error) {
    yield put(actionUpdateAvatarFailed(result.error));
  } else {
    yield put(actionUpdateAvatarSuccess(result.user));
  }
}

function* userUpdateInfo(action) {
  const { payload } = action;

  const result = yield fetch('http://twitter-clone.loc:8091/api/user/update-info', {
    method: 'POST',
    mode: 'cors',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(payload.data),
  })
    .then((response) => response.json())
    .then((resolve) => {
      if (resolve.error) {
        return {
          user: {},
          error: resolve.error,
        };
      }
      return {
        user: resolve,
        error: '',
      };
    })
    .catch((reject) => {
      return {
        user: {},
        error: reject.message,
      };
    });

  if (result.error) {
    yield put(actionUpdateInfoFailed(result.error));
  } else {
    yield put(actionUpdateInfoSuccess(result.user));
  }
}

function* userChangePass(action) {
  const { payload } = action;

  const result = yield fetch('http://twitter-clone.loc:8091/api/user/change-pass', {
    method: 'POST',
    mode: 'cors',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(payload.dataPass),
  })
    .then((response) => response.json())
    .then((resolve) => {
      if (resolve.error) {
        return {
          error: resolve.error,
        };
      }
      return {
        status: resolve.status,
      };
    })
    .catch((reject) => {
      return {
        error: reject.message,
      };
    });

  if (result.error) {
    yield put(actionChangePassFailed(result.error));
  } else {
    yield put(actionChangePassSuccess(result.status));
  }
}

export function* userWatcher() {
  yield takeLatest(USER_LOGIN_REQUEST, userLogin);
  yield takeLatest(USER_LOGOUT_REQUEST, userLogout);
  yield takeLatest(USER_UPDATE_AVATAR_REQUEST, updateAvatar);
  yield takeLatest(USER_UPDATE_INFO_REQUEST, userUpdateInfo);
  yield takeLatest(USER_CHANGE_PASS_REQUEST, userChangePass);
}
