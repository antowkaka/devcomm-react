import { put, takeLatest } from 'redux-saga/effects';

import { SEARCH_REQUEST } from '../actionTypes';
import { actionSearchFailed, actionSearchSuccess } from '../actions/searchAction';

function* searchUser(action) {
  const { payload } = action;

  const result = yield fetch(`http://twitter-clone/api/user/search/${payload.data.searchQuery}`)
    .then((response) => response.json())
    .then((resolve) => {
      if (resolve.errors) {
        return {
          responseErrors: resolve.errors,
        };
      }
      return {
        searchResult: resolve.searchResult,
        responseErrors: '',
      };
    })
    .catch((reject) => {
      return {
        responseErrors: reject.message,
      };
    });

  if (result.responseErrors) {
    yield put(actionSearchFailed(result.responseErrors));
  } else {
    yield put(actionSearchSuccess(result.searchResult));
  }
}

export function* searchWatcher() {
  yield takeLatest(SEARCH_REQUEST, searchUser);
}
