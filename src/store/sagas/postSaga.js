import { takeLatest, put } from 'redux-saga/effects';
import {
  CREATE_NEW_POST_REQUEST,
  GET_ALL_POSTS_REQUEST,
  UPDATE_POST_REQUEST,
  DELETE_POST_REQUEST,
  LIKE_POST,
  DISLIKE_POST,
} from '../actionTypes';
import {
  actionGetAllPostsSuccess,
  actionGetAllPostsFailed,
  actionCreatePostFailed,
  actionCreatePostSuccess,
  actionUpdatePostFailed,
  actionUpdatePostSuccess,
  actionDeletePostFailed,
  actionDeletePostSuccess,
} from '../actions/postAction';

function* getAllPosts(action) {
  const { payload } = action;
  const result = yield fetch(`http://twitter-clone.loc:8091/api/post/read-all/${payload}`)
    .then((response) => response.json())
    .then((posts) => {
      return {
        posts,
        error: '',
      };
    })
    .catch((error) => {
      return {
        posts: [],
        error,
      };
    });

  if (result.error) {
    yield put(actionGetAllPostsFailed(result.error.message));
  } else {
    yield put(actionGetAllPostsSuccess(result.posts));
  }
}

function* createNewPost(action) {
  const { payload } = action;

  const postData = new FormData();
  postData.append('userId', payload.data.userId);
  postData.append('postText', payload.data.text);
  postData.append('postImg', payload.data.postImg);

  const result = yield fetch('http://twitter-clone.loc:8091/api/post/create', {
    method: 'POST',
    body: postData,
  })
    .then((response) => response.json())
    .then((posts) => {
      if (posts.error) {
        return {
          error: posts.error,
        };
      }
      return {
        posts,
      };
    })
    .catch((error) => {
      return {
        error: error.message,
      };
    });

  if (result.error) {
    console.log(result.error);
    yield put(actionCreatePostFailed(result.error));
  } else {
    yield put(actionCreatePostSuccess(result.posts));
  }
}

function* updatePost(action) {
  const { payload } = action;
  const result = yield fetch('http://twitter-clone.loc:8091/api/post/update', {
    method: 'POST',
    mode: 'cors',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(payload),
  })
    .then((response) => response.json())
    .then((posts) => {
      if (posts.error) {
        return {
          error: posts.error,
        };
      }
      return {
        posts,
        error: '',
      };
    })
    .catch((error) => {
      return {
        error: error.message,
      };
    });
  if (result.error) {
    yield put(actionUpdatePostFailed(result.error));
  } else {
    yield put(actionUpdatePostSuccess(result.posts));
  }
}

function* deletePost(action) {
  const { payload } = action;
  const result = yield fetch(`http://twitter-clone.loc:8091/api/post/delete/${payload}`)
    .then((response) => response.json())
    .then((resolve) => {
      if (resolve.error) {
        return {
          error: resolve.error,
        };
      }
      return {
        status: resolve.status,
      };
    })
    .catch((error) => {
      return {
        error: error.message,
      };
    });

  if (result.error) {
    yield put(actionDeletePostFailed(result.error));
  } else {
    yield put(actionDeletePostSuccess(result.resolve));
  }
}

function* likePost(action) {
  const { payload } = action;
  yield fetch(
    `http://twitter-clone.loc:8091/api/like/create/?post_id=${payload.postId}&user_id=${payload.userId}`,
  );
}

function* dislikePost(action) {
  const { payload } = action;
  yield fetch(
    `http://twitter-clone.loc:8091/api/like/delete/?post_id=${payload.postId}&user_id=${payload.userId}`,
  );
}

export function* postWatcher() {
  yield takeLatest(GET_ALL_POSTS_REQUEST, getAllPosts);
  yield takeLatest(CREATE_NEW_POST_REQUEST, createNewPost);
  yield takeLatest(UPDATE_POST_REQUEST, updatePost);
  yield takeLatest(DELETE_POST_REQUEST, deletePost);
  yield takeLatest(LIKE_POST, likePost);
  yield takeLatest(DISLIKE_POST, dislikePost);
}
