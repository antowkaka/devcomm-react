import { put, takeLatest } from 'redux-saga/effects';

import { GET_FOLLOWERS_REQUEST } from '../actionTypes';
import { actionGetFollowersFailed, actionGetFollowersSuccess } from '../actions/followersAction';

function* getFollowers(action) {
  const { payload } = action;

  const result = yield fetch(
    `http://twitter-clone.loc:8091/api/user/get-all-followers/${payload.data}`,
  )
    .then((response) => response.json())
    .then((resolve) => {
      if (resolve.errors) {
        return {
          responseErrors: resolve.errors,
        };
      }
      return {
        followers: resolve.followers,
        responseErrors: '',
      };
    })
    .catch((reject) => {
      return {
        responseErrors: reject.message,
      };
    });

  if (result.responseErrors) {
    yield put(actionGetFollowersFailed(result.responseErrors));
  } else {
    yield put(actionGetFollowersSuccess(result.followers));
  }
}

export function* followersWatcher() {
  yield takeLatest(GET_FOLLOWERS_REQUEST, getFollowers);
}
