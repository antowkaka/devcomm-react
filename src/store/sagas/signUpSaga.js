import { takeLatest, put } from 'redux-saga/effects';
import { SIGNUP_REQUEST } from '../actionTypes';
import { actionSignUpSuccess, actionSignUpFailed } from '../actions/signUpAction';

function* signUp(action) {
  const { payload } = action;

  const result = yield fetch('http://twitter-clone/api/user/signup', {
    method: 'POST',
    mode: 'cors',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(payload.data),
  })
    .then((response) => response.json())
    .then((resolve) => {
      if (resolve.errors) {
        return {
          responseErrors: resolve.errors,
        };
      }
      return {
        isSignUpSuccess: resolve.status,
        responseErrors: '',
      };
    })
    .catch((reject) => {
      return {
        isSignUpSuccess: false,
        responseErrors: reject.message,
      };
    });

  if (result.responseErrors) {
    yield put(actionSignUpFailed(result.responseErrors));
  } else {
    yield put(actionSignUpSuccess(result.isSignUpSuccess));
  }
}

export function* signUpWatcher() {
  yield takeLatest(SIGNUP_REQUEST, signUp);
}
