import { put, takeLatest } from 'redux-saga/effects';

import { GET_ALL_USERS_REQUEST } from '../actionTypes';
import { actionGetAllUsersSuccess, actionGetAllUsersFailed } from '../actions/allUsersAction';

function* getAllUsers() {
  const result = yield fetch(`http://twitter-clone.loc:8091/api/user/read`)
    .then((response) => response.json())
    .then((resolve) => {
      if (resolve.error) {
        return {
          responseErrors: resolve.error,
        };
      }
      return {
        allUsers: resolve.allUsers,
        responseErrors: '',
      };
    })
    .catch((reject) => {
      return {
        responseErrors: reject.message,
      };
    });

  if (result.responseErrors) {
    yield put(actionGetAllUsersFailed(result.responseErrors));
  } else {
    yield put(actionGetAllUsersSuccess(result.allUsers));
  }
}

export function* allUsersWatcher() {
  yield takeLatest(GET_ALL_USERS_REQUEST, getAllUsers);
}
