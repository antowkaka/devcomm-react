import { SEARCH_FAILED, SEARCH_REQUEST, SEARCH_SUCCESS } from '../actionTypes';

export function actionSearchRequest(data) {
  return {
    type: SEARCH_REQUEST,
    payload: {
      data,
    },
  };
}

export function actionSearchSuccess(payload) {
  return {
    type: SEARCH_SUCCESS,
    payload,
  };
}

export function actionSearchFailed(payload) {
  return {
    type: SEARCH_FAILED,
    payload,
  };
}
