import { SIGNUP_ERROR, SIGNUP_REQUEST, SIGNUP_SUCCESS } from '../actionTypes';

export function actionSignUpRequest(data) {
  return {
    type: SIGNUP_REQUEST,
    payload: {
      data,
    },
  };
}

export function actionSignUpSuccess(payload) {
  return {
    type: SIGNUP_SUCCESS,
    payload,
  };
}

export function actionSignUpFailed(payload) {
  return {
    type: SIGNUP_ERROR,
    payload,
  };
}
