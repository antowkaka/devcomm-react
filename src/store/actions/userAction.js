import {
  USER_LOGIN_REQUEST,
  USER_LOGIN_SUCCESS,
  USER_LOGIN_FAILED,
  USER_LOGOUT_REQUEST,
  USER_LOGOUT_SUCCESS,
  USER_LOGOUT_FAILED,
  USER_UPDATE_AVATAR_REQUEST,
  USER_UPDATE_AVATAR_SUCCESS,
  USER_UPDATE_AVATAR_FAILED,
  USER_UPDATE_INFO_REQUEST,
  USER_UPDATE_INFO_SUCCESS,
  USER_UPDATE_INFO_FAILED,
  USER_CHANGE_PASS_REQUEST,
  USER_CHANGE_PASS_SUCCESS,
  USER_CHANGE_PASS_FAILED,
  ADD_ANOTHER_USER,
} from '../actionTypes';

export function actionUserLoginRequest(data) {
  return {
    type: USER_LOGIN_REQUEST,
    payload: {
      data,
    },
  };
}

export function actionUserLoginSuccess(payload) {
  return {
    type: USER_LOGIN_SUCCESS,
    payload,
  };
}

export function actionUserLoginFailed(payload) {
  return {
    type: USER_LOGIN_FAILED,
    payload,
  };
}

export function actionUserLogoutRequest() {
  return {
    type: USER_LOGOUT_REQUEST,
  };
}

export function actionUserLogoutSuccess(payload) {
  return {
    type: USER_LOGOUT_SUCCESS,
    payload,
  };
}

export function actionUserLogoutFailed(payload) {
  return {
    type: USER_LOGOUT_FAILED,
    payload,
  };
}

export function actionUpdateAvatarRequest(data) {
  return {
    type: USER_UPDATE_AVATAR_REQUEST,
    payload: {
      data,
    },
  };
}

export function actionUpdateAvatarSuccess(payload) {
  return {
    type: USER_UPDATE_AVATAR_SUCCESS,
    payload,
  };
}

export function actionUpdateAvatarFailed(payload) {
  return {
    type: USER_UPDATE_AVATAR_FAILED,
    payload,
  };
}

export function actionUpdateInfoRequest(data) {
  return {
    type: USER_UPDATE_INFO_REQUEST,
    payload: {
      data,
    },
  };
}

export function actionUpdateInfoSuccess(payload) {
  return {
    type: USER_UPDATE_INFO_SUCCESS,
    payload,
  };
}

export function actionUpdateInfoFailed(payload) {
  return {
    type: USER_UPDATE_INFO_FAILED,
    payload,
  };
}

export function actionChangePassRequest(dataPass) {
  return {
    type: USER_CHANGE_PASS_REQUEST,
    payload: {
      dataPass,
    },
  };
}

export function actionChangePassSuccess(payload) {
  return {
    type: USER_CHANGE_PASS_SUCCESS,
    payload,
  };
}

export function actionChangePassFailed(payload) {
  return {
    type: USER_CHANGE_PASS_FAILED,
    payload,
  };
}

export function addAnotherUser(payload) {
  return {
    type: ADD_ANOTHER_USER,
    payload,
  };
}
