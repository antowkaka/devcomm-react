import {
  DELETE_FROM_WHOTOFOLLOW,
  GET_ALL_USERS_FAILED,
  GET_ALL_USERS_REQUEST,
  GET_ALL_USERS_SUCCESS,
} from '../actionTypes';

export function actionGetAllUsersRequest() {
  return {
    type: GET_ALL_USERS_REQUEST,
  };
}

export function actionGetAllUsersSuccess(payload) {
  return {
    type: GET_ALL_USERS_SUCCESS,
    payload,
  };
}

export function actionGetAllUsersFailed(payload) {
  return {
    type: GET_ALL_USERS_FAILED,
    payload,
  };
}

export function actionDeleteFromWhoToFollow(payload) {
  return {
    type: DELETE_FROM_WHOTOFOLLOW,
    payload,
  };
}
