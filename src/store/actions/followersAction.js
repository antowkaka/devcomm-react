import { GET_FOLLOWERS_FAILED, GET_FOLLOWERS_REQUEST, GET_FOLLOWERS_SUCCESS } from '../actionTypes';

export function actionGetFollowersRequest(data) {
  return {
    type: GET_FOLLOWERS_REQUEST,
    payload: {
      data,
    },
  };
}

export function actionGetFollowersSuccess(payload) {
  return {
    type: GET_FOLLOWERS_SUCCESS,
    payload,
  };
}

export function actionGetFollowersFailed(payload) {
  return {
    type: GET_FOLLOWERS_FAILED,
    payload,
  };
}
