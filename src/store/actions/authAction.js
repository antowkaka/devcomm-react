import { CHECK_AUTH, CHECK_AUTH_SUCCESS, CHECK_AUTH_FAILED } from '../actionTypes';

export function checkAuth() {
  return {
    type: CHECK_AUTH,
  };
}

export function checkAuthSuccess(status) {
  return {
    type: CHECK_AUTH_SUCCESS,
    payload: { status },
  };
}

export function checkAuthFailed(status) {
  return {
    type: CHECK_AUTH_FAILED,
    payload: { status },
  };
}
