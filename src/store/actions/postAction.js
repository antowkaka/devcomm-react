import {
  GET_ALL_POSTS_FAILED,
  GET_ALL_POSTS_REQUEST,
  GET_ALL_POSTS_SUCCESS,
  CREATE_NEW_POST_FAILED,
  CREATE_NEW_POST_REQUEST,
  CREATE_NEW_POST_SUCCESS,
  UPDATE_POST_REQUEST,
  UPDATE_POST_SUCCESS,
  UPDATE_POST_FAILED,
  DELETE_POST_REQUEST,
  DELETE_POST_SUCCESS,
  DELETE_POST_FAILED,
  LIKE_POST,
  DISLIKE_POST,
} from '../actionTypes';

export function actionGetAllPostsRequest(payload) {
  return {
    type: GET_ALL_POSTS_REQUEST,
    payload,
  };
}

export function actionGetAllPostsSuccess(payload) {
  return {
    type: GET_ALL_POSTS_SUCCESS,
    payload,
  };
}

export function actionGetAllPostsFailed(payload) {
  return {
    type: GET_ALL_POSTS_FAILED,
    payload,
  };
}

export function actionCreatePostRequest(data) {
  return {
    type: CREATE_NEW_POST_REQUEST,
    payload: {
      data,
    },
  };
}

export function actionCreatePostSuccess(payload) {
  return {
    type: CREATE_NEW_POST_SUCCESS,
    payload,
  };
}

export function actionCreatePostFailed(payload) {
  return {
    type: CREATE_NEW_POST_FAILED,
    payload,
  };
}

export function actionUpdatePostRequest(data) {
  return {
    type: UPDATE_POST_REQUEST,
    payload: data,
  };
}

export function actionUpdatePostSuccess(payload) {
  return {
    type: UPDATE_POST_SUCCESS,
    payload,
  };
}

export function actionUpdatePostFailed(payload) {
  return {
    type: UPDATE_POST_FAILED,
    payload,
  };
}

export function actionDeletePostRequest(payload) {
  return {
    type: DELETE_POST_REQUEST,
    payload,
  };
}

export function actionDeletePostSuccess(payload) {
  return {
    type: DELETE_POST_SUCCESS,
    payload,
  };
}

export function actionDeletePostFailed(payload) {
  return {
    type: DELETE_POST_FAILED,
    payload,
  };
}

export function actionLikePost(likeData) {
  return {
    type: LIKE_POST,
    payload: likeData,
  };
}

export function actionDislikePost(likeData) {
  return {
    type: DISLIKE_POST,
    payload: likeData,
  };
}
