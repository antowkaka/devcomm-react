import {
  DELETE_FROM_WHOTOFOLLOW,
  GET_ALL_USERS_FAILED,
  GET_ALL_USERS_SUCCESS,
} from '../actionTypes';

const initialState = {
  allUsers: [],
  whoToFollow: [],
  responseErrors: {},
  isLoading: true,
};

export function allUsersReducer(state = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case GET_ALL_USERS_SUCCESS:
      return {
        ...state,
        ...{
          isLoading: false,
          allUsers: payload,
          whoToFollow: payload,
        },
      };
    case GET_ALL_USERS_FAILED:
      return {
        ...state,
        ...{
          responseErrors: payload,
          isLoading: false,
        },
      };
    case DELETE_FROM_WHOTOFOLLOW:
      return {
        ...state,
        ...{
          whoToFollow: payload,
          isLoading: false,
        },
      };
    default:
      return state;
  }
}
