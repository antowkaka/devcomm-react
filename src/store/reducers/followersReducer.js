import { GET_FOLLOWERS_FAILED, GET_FOLLOWERS_SUCCESS } from '../actionTypes';

const initialState = {
  followers: [],
  responseErrors: {},
  isLoading: true,
};

export function followersReducer(state = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case GET_FOLLOWERS_SUCCESS:
      return {
        ...state,
        ...{
          isLoading: false,
          followers: payload,
        },
      };
    case GET_FOLLOWERS_FAILED:
      return {
        ...state,
        ...{
          responseErrors: payload,
          isLoading: false,
        },
      };
    default:
      return state;
  }
}
