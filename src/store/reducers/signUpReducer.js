import { SIGNUP_ERROR, SIGNUP_SUCCESS } from '../actionTypes';

const initialState = {
  responseErrors: {},
  isLoading: true,
  isSignUpSuccess: false,
};

export function signUpReducer(state = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case SIGNUP_SUCCESS:
      return {
        ...state,
        ...{
          isLoading: false,
          isSignUpSuccess: payload,
        },
      };
    case SIGNUP_ERROR:
      return {
        ...state,
        ...{
          responseErrors: payload,
          isLoading: false,
        },
      };
    default:
      return state;
  }
}
