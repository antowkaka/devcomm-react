import {
  FOLLOW_FAILED,
  FOLLOW_SUCCESS,
  GET_FOLLOWING_FAILED,
  GET_FOLLOWING_SUCCESS,
  UNFOLLOW_FAILED,
  UNFOLLOW_SUCCESS,
} from '../actionTypes';

const initialState = {
  following: [],
  responseErrors: {},
  isLoading: true,
};

export function followingReducer(state = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case GET_FOLLOWING_SUCCESS:
      return {
        ...state,
        ...{
          isLoading: false,
          following: payload,
        },
      };
    case GET_FOLLOWING_FAILED:
      return {
        ...state,
        ...{
          responseErrors: payload,
          isLoading: false,
        },
      };
    case FOLLOW_SUCCESS:
      return {
        ...state,
        ...{
          isLoading: false,
          following: payload,
        },
      };
    case FOLLOW_FAILED:
      return {
        ...state,
        ...{
          responseErrors: payload,
          isLoading: false,
        },
      };
    case UNFOLLOW_SUCCESS:
      return {
        ...state,
        ...{
          isLoading: false,
          following: payload,
        },
      };
    case UNFOLLOW_FAILED:
      return {
        ...state,
        ...{
          responseErrors: payload,
          isLoading: false,
        },
      };
    default:
      return state;
  }
}
