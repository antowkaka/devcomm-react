import {
  USER_LOGIN_SUCCESS,
  USER_LOGIN_FAILED,
  CHECK_AUTH_SUCCESS,
  CHECK_AUTH_FAILED,
  USER_LOGOUT_SUCCESS,
  USER_LOGOUT_FAILED,
  USER_UPDATE_AVATAR_SUCCESS,
  USER_UPDATE_AVATAR_FAILED,
  USER_UPDATE_INFO_SUCCESS,
  USER_UPDATE_INFO_FAILED,
  USER_CHANGE_PASS_SUCCESS,
  USER_CHANGE_PASS_FAILED,
  ADD_ANOTHER_USER,
} from '../actionTypes';

const initialState = {
  user: {},
  error: '',
  isLoading: true,
  hasAuthenticated: false,
  authError: '',
  anotherUser: {},
};

export function userReducer(state = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case USER_LOGIN_SUCCESS:
      return {
        ...state,
        ...{
          user: payload,
          error: '',
          isLoading: false,
          hasAuthenticated: true,
        },
      };
    case USER_LOGIN_FAILED:
      return {
        ...state,
        ...{
          user: {},
          error: payload,
          isLoading: false,
        },
      };
    case CHECK_AUTH_SUCCESS:
      return {
        ...state,
        ...{
          user: payload.status.user,
          authError: payload.status.error,
          isLoading: false,
          hasAuthenticated: payload.status.status,
        },
      };
    case CHECK_AUTH_FAILED:
      return {
        ...state,
        ...{
          authError: payload.status,
          isLoading: false,
        },
      };
    case USER_LOGOUT_SUCCESS:
      return {
        ...state,
        ...{
          user: {},
          error: '',
          isLoading: false,
          hasAuthenticated: payload.status,
          authError: '',
        },
      };
    case USER_LOGOUT_FAILED:
      return {
        ...state,
        ...{
          error: payload.error,
        },
      };
    case USER_UPDATE_AVATAR_SUCCESS:
      return {
        ...state,
        ...{
          user: payload,
          error: '',
          isLoading: false,
          hasAuthenticated: true,
        },
      };
    case USER_UPDATE_AVATAR_FAILED:
      return {
        ...state,
        ...{
          error: payload,
        },
      };
    case USER_UPDATE_INFO_SUCCESS:
      return {
        ...state,
        ...{
          user: payload,
          status: 'Success!',
          alert: 'success',
          isLoading: false,
          hasAuthenticated: true,
        },
      };
    case USER_UPDATE_INFO_FAILED:
      return {
        ...state,
        ...{
          status: payload,
          alert: 'danger',
          isLoading: false,
        },
      };
    case 'DELETE_STATUS':
      return {
        ...state,
        ...{
          status: payload,
          alert: 'danger',
          isLoading: false,
        },
      };
    case USER_CHANGE_PASS_SUCCESS:
      return {
        ...state,
        ...{
          status: payload,
          alert: 'success',
          isLoading: false,
        },
      };
    case USER_CHANGE_PASS_FAILED:
      return {
        ...state,
        ...{
          error: payload,
          alert: 'danger',
          isLoading: false,
        },
      };
    case ADD_ANOTHER_USER:
      return {
        ...state,
        ...{
          anotherUser: payload,
        },
      };
    default:
      return state;
  }
}
