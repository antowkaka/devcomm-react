import { SEARCH_SUCCESS, SIGNUP_ERROR } from '../actionTypes';

const initialState = {
  searchResult: [],
  responseErrors: {},
  isLoading: true,
};

export function searchReducer(state = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case SEARCH_SUCCESS:
      return {
        ...state,
        ...{
          isLoading: false,
          searchResult: payload,
        },
      };
    case SIGNUP_ERROR:
      return {
        ...state,
        ...{
          responseErrors: payload,
          isLoading: false,
        },
      };
    default:
      return state;
  }
}
