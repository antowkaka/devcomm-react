import {
  GET_ALL_POSTS_FAILED,
  GET_ALL_POSTS_SUCCESS,
  CREATE_NEW_POST_FAILED,
  CREATE_NEW_POST_SUCCESS,
  UPDATE_POST_SUCCESS,
  UPDATE_POST_FAILED,
  DELETE_POST_SUCCESS,
  DELETE_POST_FAILED,
} from '../actionTypes';

const initialState = {
  posts: [],
  error: '',
  isLoading: true,
  postForm: {
    userId: null,
    text: null,
    img: null,
    error: '',
  },
};

export function postReducer(state = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case GET_ALL_POSTS_SUCCESS:
      return {
        ...state,
        ...{
          posts: payload,
          error: '',
          isLoading: false,
        },
      };
    case GET_ALL_POSTS_FAILED:
      return {
        ...state,
        ...{
          posts: [],
          error: payload,
          isLoading: false,
        },
      };
    case CREATE_NEW_POST_SUCCESS:
      return {
        ...state,
        ...{
          posts: payload,
          isLoading: false,
        },
      };
    case CREATE_NEW_POST_FAILED:
      return {
        ...state,
        ...{
          error: payload,
          isLoading: false,
        },
      };
    case UPDATE_POST_SUCCESS:
      return {
        ...state,
        ...{
          posts: payload,
          isLoading: false,
        },
      };
    case UPDATE_POST_FAILED:
      return {
        ...state,
        ...{
          error: payload,
          isLoading: false,
        },
      };
    case DELETE_POST_SUCCESS:
      return {
        ...state,
        ...{
          status: payload,
          isLoading: false,
        },
      };
    case DELETE_POST_FAILED:
      return {
        ...state,
        ...{
          error: payload,
          isLoading: false,
        },
      };
    default:
      return state;
  }
}
