import React, { useEffect } from 'react';
import { Route, Switch, Redirect, useLocation } from 'react-router-dom';
import { useDispatch } from 'react-redux';

import { StartPage } from './pages/StartPage/StartPage';
import { HomePage } from './pages/HomePage/HomePage';
import { LoginForm } from './components/Forms/LoginForm';
import { RegistrationForm } from './components/Forms/RegistrationForm';
import { UserProfile } from './pages/UserProfile/UserProfile';
import { EditProfile } from './pages/EditProfile/EditProfile';
import { SearchPage } from './pages/SearchPage/SearchPage';
import useShallowEqualSelector from './hooks/useShallowEqualSelector';
import { checkAuth } from './store/actions/authAction';
import { AnotherUserProfile } from './pages/AnotherUserProfile/AnotherUserProfile';

function App() {
  const { hasAuthenticated } = useShallowEqualSelector((state) => state.user);
  const dispatch = useDispatch();
  const location = useLocation();

  useEffect(() => {
    dispatch(checkAuth());
  }, [location]);

  return (
    <>
      <Switch>
        <Route path="/home">{hasAuthenticated ? <HomePage /> : <Redirect to="/" />}</Route>
        <Route path="/login">{hasAuthenticated ? <Redirect to="/home" /> : <LoginForm />}</Route>
        <Route path="/registration">
          {hasAuthenticated ? <Redirect to="/home" /> : <RegistrationForm />}
        </Route>
        <Route path="/profile">{hasAuthenticated ? <UserProfile /> : <Redirect to="/" />}</Route>
        <Route path="/user">
          {hasAuthenticated ? <AnotherUserProfile /> : <Redirect to="/" />}
        </Route>
        <Route path="/edit">{hasAuthenticated ? <EditProfile /> : <Redirect to="/" />}</Route>
        <Route path="/search">{hasAuthenticated ? <SearchPage /> : <Redirect to="/" />}</Route>
        <Route path="/">{hasAuthenticated ? <Redirect to="/home" /> : <StartPage />}</Route>
      </Switch>
    </>
  );
}

export default App;
