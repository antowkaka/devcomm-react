import React, { useEffect } from 'react';
import '../UserProfile/UserProfile.scss';
import { Tabs, TabLink, TabContent } from 'react-tabs-redux';
import { useLocation } from 'react-router-dom';
import { useDispatch } from 'react-redux';

import { Header } from '../../components/Header/Header';
import useShallowEqualSelector from '../../hooks/useShallowEqualSelector';
import { FollowCard } from '../../components/FollowCard/FollowCard';
import { PostComponent } from '../../components/PostComponent/PostComponent';
import { actionGetFollowersRequest } from '../../store/actions/followersAction';
import { actionGetFollowingRequest } from '../../store/actions/followingAction';

export const AnotherUserProfile = () => {
  const { anotherUser } = useShallowEqualSelector((state) => state.user);
  const { posts, isLoading } = useShallowEqualSelector((state) => state.post);
  const location = useLocation();
  const userName = location.pathname.split('/')[2];
  const userPosts = posts.filter((post) => post.username === userName);
  const { followers } = useShallowEqualSelector((state) => state.followers);
  const { following } = useShallowEqualSelector((state) => state.following);
  const { allUsers } = useShallowEqualSelector((state) => state.allUsers);
  const { bio } = allUsers.filter((user) => user.username === userName)[0];
  const dispatch = useDispatch();
  const followingCount = following ? following.length : 0;
  const followersCount = followers ? followers.length : 0;
  const avatar = `http://twitter-clone.loc:8091/${anotherUser.profileimg}`;

  useEffect(() => {
    dispatch(actionGetFollowersRequest(anotherUser.id));
    dispatch(actionGetFollowingRequest(anotherUser.id));
  }, []);

  return (
    <>
      <Header newClass="header" />

      <main className="UserProfile">
        <section className="container-fluid tabs">
          <div className="container">
            <div className="row">
              <div className="col-9 ml-auto tabs__wrapper" />
            </div>
          </div>
        </section>
        <div className="container">
          <div className="row pt-3">
            <section className="sidebar col-lg-3 p-md-0">
              {anotherUser.profileimg ? (
                <img src={avatar} alt="User avatar" className="user-photo" />
              ) : (
                <div className="user-photo" />
              )}

              <div className="card user-info">
                <div className="card-body d-flex flex-column">
                  <h4 className="card-title user-info__title mt-2">
                    {anotherUser.firstname} {anotherUser.lastname}
                  </h4>
                  <h6 className="card-subtitle mb-2 text-muted user-info__subtitle">
                    @{anotherUser.username}
                  </h6>
                  <p className="card-text user-info__text">
                    {bio || 'Edit your profile to add bio'}
                  </p>
                </div>
              </div>
            </section>
            <section className="main-column col-lg-6 p-md-0">
              <Tabs>
                <div className="tabs-wrapper">
                  <div className="col-9 tabs__group tabs__group--another">
                    <TabLink to="tab1" activeClassName="tabs__info--active" className="tabs__info">
                      <span className="tabs__title">Posts</span>
                      <span className="tabs__quantity">{userPosts.length}</span>
                    </TabLink>
                    <TabLink to="tab2" activeClassName="tabs__info--active" className="tabs__info">
                      <span className="tabs__title">Following</span>
                      <span className="tabs__quantity">{followingCount}</span>
                    </TabLink>

                    <TabLink to="tab3" activeClassName="tabs__info--active" className="tabs__info">
                      <span className="tabs__title">Followers</span>
                      <span className="tabs__quantity">{followersCount}</span>
                    </TabLink>
                  </div>

                  <TabContent for="tab1">
                    <section className="tweets-tab active-tab">
                      {!isLoading && userPosts.length > 0 ? (
                        userPosts.map((post) => (
                          <PostComponent
                            key={post.id}
                            id={post.id}
                            firstName={post.firstname}
                            lastName={post.lastname}
                            username={post.username}
                            likes={post.likes}
                            isLike={post.isLike}
                            text={post.body}
                            imgLink={post.post_img}
                            date={post.created_at}
                            avatarLink={post.profileimg}
                          />
                        ))
                      ) : (
                        <p className="no-tweets">No posts from you yet.</p>
                      )}
                    </section>
                  </TabContent>
                  <TabContent for="tab2">
                    <section className="following-tab">
                      {!following && (
                        <p className="no-following">You have not followed anybody yet.</p>
                      )}

                      <div className="following-tab-wrapper d-flex flex-wrap justify-content-between">
                        {following &&
                          following.map((user) => {
                            const inFollowing = following.filter((item) => item.id === user.id);
                            const isAlreadyFollowing = inFollowing.length > 0;

                            const inFollowers = followers
                              ? followers.filter((item) => item.id === user.id)
                              : [];
                            const isFollower =
                              inFollowers.length > 0 ? 'Follows you' : 'Not follows you';

                            return (
                              <FollowCard
                                key={user.id}
                                firstName={user.firstname}
                                lastName={user.lastname}
                                username={user.username}
                                avatarLink={user.profileimg}
                                userID={user.id}
                                isAlreadyFollowing={isAlreadyFollowing}
                                isFollower={isFollower}
                                hiddenBtn
                              />
                            );
                          })}
                      </div>
                    </section>
                  </TabContent>
                  <TabContent for="tab3">
                    <section className="followers-tab">
                      {!followers && <p className="no-followers">No followers yet.</p>}

                      <div className="following-tab-wrapper d-flex flex-wrap justify-content-between">
                        {followers &&
                          followers.map((user) => {
                            const results = following
                              ? following.filter((item) => item.id === user.id)
                              : [];

                            const isAlreadyFollowing = results.length > 0;

                            return (
                              <FollowCard
                                firstName={user.firstname}
                                lastName={user.lastname}
                                username={user.username}
                                avatarLink={user.profileimg}
                                userID={user.id}
                                isAlreadyFollowing={isAlreadyFollowing}
                                hiddenBtn
                              />
                            );
                          })}
                      </div>
                    </section>
                  </TabContent>
                </div>
              </Tabs>
            </section>
          </div>
        </div>
      </main>
    </>
  );
};
