import React from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import { Header } from '../../components/Header/Header';
import { UserCard } from '../../components/UserCard/UserCard';
import './EditProfile.scss';
import { EditProfileForm } from '../../components/Forms/EditProfileForm';
import { ChangePassForm } from '../../components/Forms/ChangePassForm';
import useShallowEqualSelector from '../../hooks/useShallowEqualSelector';

export function EditProfile() {
  const { user } = useShallowEqualSelector((state) => state.user);

  return (
    <>
      <Header />
      <main className="EditProfile pt-4">
        <Container>
          <Row className="pt-lg-0 pt-md-3">
            <Col lg={3} className="HomePage_left_sidebar p-md-2">
              <UserCard
                firstName={user.firstname}
                lastName={user.lastname}
                username={user.username}
                avatarLink={user.profileimg}
              />
            </Col>
            <Col lg={6} className="HomePage_posts p-md-2">
              <EditProfileForm />
              <ChangePassForm />
            </Col>
          </Row>
        </Container>
      </main>
    </>
  );
}
