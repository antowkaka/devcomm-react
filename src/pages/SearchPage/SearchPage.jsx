import React from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import { Header } from '../../components/Header/Header';
import { UserCard } from '../../components/UserCard/UserCard';
import { SearchResultUser } from '../../components/SearchResultUser/SearchResultUser';
import useShallowEqualSelector from '../../hooks/useShallowEqualSelector';

export const SearchPage = () => {
  const { searchResult } = useShallowEqualSelector((state) => state.search);
  const { user } = useShallowEqualSelector((state) => state.user);
  const { following } = useShallowEqualSelector((state) => state.following);

  return (
    <>
      <Header />

      <main className="HomePage pt-4">
        <Container>
          <Row className="pt-lg-0 pt-md-3">
            <Col lg={3} className=" p-md-2">
              <UserCard
                firstName={user.firstname}
                lastName={user.lastname}
                username={user.username}
                avatarLink={user.profileimg}
              />
            </Col>
            <Col lg={6} className="p-md-2">
              {searchResult &&
                searchResult.map((user) => {
                  const inFollowing = following
                    ? following.filter((item) => item.id === user.id)
                    : [];
                  const isAlreadyFollowing = inFollowing.length > 0;

                  return (
                    <SearchResultUser
                      firstName={user.firstname}
                      lastName={user.lastname}
                      username={user.username}
                      avatarLink={user.profileimg}
                      userID={user.id}
                      isAlreadyFollowing={isAlreadyFollowing}
                    />
                  );
                })}

              {!searchResult && (
                <>
                  <p className="not-found-user">
                    We did not find anyone matching the request.
                    <span role="img" aria-label="sad-face">
                      &#128546;
                    </span>
                  </p>

                  <p className="not-found-user">Please try again.</p>
                </>
              )}
            </Col>
          </Row>
        </Container>
      </main>
    </>
  );
};
