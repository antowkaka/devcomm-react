import React, { useEffect } from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import { useDispatch } from 'react-redux';

import { actionGetAllPostsRequest } from '../../store/actions/postAction';
import { Header } from '../../components/Header/Header';
import { UserCard } from '../../components/UserCard/UserCard';
import { PostForm } from '../../components/Forms/PostForm';
import './HomePage.scss';
import { PostComponent } from '../../components/PostComponent/PostComponent';
import { RecommendsBlock } from '../../components/RecommendsBlock/RecommendsBlock';
import { CopyrightBlock } from '../../components/CopyrightBlock/CopyrightBlock';
import useShallowEqualSelector from '../../hooks/useShallowEqualSelector';
import { actionGetAllUsersRequest } from '../../store/actions/allUsersAction';
import { actionGetFollowersRequest } from '../../store/actions/followersAction';
import { actionGetFollowingRequest } from '../../store/actions/followingAction';

export function HomePage() {
  const { user } = useShallowEqualSelector((state) => state.user);
  const { posts, isLoading } = useShallowEqualSelector((state) => state.post);
  const { whoToFollow } = useShallowEqualSelector((state) => state.allUsers);

  const dispatch = useDispatch();

  useEffect(() => {
    if (whoToFollow.length === 0) {
      dispatch(actionGetAllUsersRequest());
    }

    dispatch(actionGetAllPostsRequest(user.id));
    dispatch(actionGetFollowersRequest(user.id));
    dispatch(actionGetFollowingRequest(user.id));
  }, []);

  return (
    <>
      <Header />
      <main className="HomePage pt-4">
        <Container>
          <Row className="pt-lg-0 pt-md-3">
            <Col lg={3} className="HomePage_left_sidebar p-md-2">
              <UserCard
                firstName={user.firstname}
                lastName={user.lastname}
                username={user.username}
                avatarLink={user.profileimg}
                postCount={user.postsCount}
                followingCount={user.subscriptionsCount}
              />
            </Col>
            <Col lg={6} className="HomePage_posts p-md-2">
              <PostForm userId={user.id} />
              {!isLoading &&
                posts.map((post) => (
                  <PostComponent
                    key={post.id}
                    id={post.id}
                    userId={post.user_id}
                    firstName={post.firstname}
                    lastName={post.lastname}
                    username={post.username}
                    likes={post.likes}
                    isLike={post.isLike}
                    text={post.body}
                    imgLink={post.post_img}
                    date={post.created_at}
                    avatarLink={post.profileimg}
                  />
                ))}
            </Col>
            <section className="col-md-3 right-sidebar">
              <RecommendsBlock />
              <CopyrightBlock />
            </section>
          </Row>
        </Container>
      </main>
    </>
  );
}
