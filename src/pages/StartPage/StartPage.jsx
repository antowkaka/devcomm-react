import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';

import './StartPage.scss';
import logo from '../../../public/img/icons/dev-comm-logo.svg';
import { StartButton } from '../../components/StartButton/StartButton';

export function StartPage() {
  return (
    <div className="StartPage">
      <Container className="StartContainer">
        <Row className="justify-content-center m-auto">
          <Row className="align-items-center p-0" lg={8} md={8} sm={12}>
            <Col>
              <img className="home-logo mr-auto" src={logo} width="45" height="45" alt="home" />
              <Col as="h1" xs={11} className="StartPage_title p-0">
                dev
                <span className="StartPage_braces-logo">&#123;</span>
                <span className="StartPage_edu-logo">education</span>
                <span className="StartPage_braces-logo">&#125; </span>
                students online home
              </Col>
              <span className="tw">
                Join
                <b> dev</b>
                <span className="StartPage_braces-logo">&#123;</span>
                <span className="StartPage_comm-logo">communication</span>
                <span className="StartPage_braces-logo">&#125; </span>
                today
              </span>

              <div className="button-wrapper">
                <StartButton text="Sign Up" path="/registration" />
                <StartButton text="Login" path="/login" />
              </div>
            </Col>
          </Row>
        </Row>
      </Container>
    </div>
  );
}
