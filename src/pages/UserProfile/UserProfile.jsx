import React, { useState } from 'react';
import './UserProfile.scss';
import { Tabs, TabLink, TabContent } from 'react-tabs-redux';
import { Link } from 'react-router-dom';
import { useDispatch } from 'react-redux';

import { useForm } from 'react-hook-form';
import { Header } from '../../components/Header/Header';
import { PostForm } from '../../components/Forms/PostForm';
import { RecommendsBlock } from '../../components/RecommendsBlock/RecommendsBlock';
import { CopyrightBlock } from '../../components/CopyrightBlock/CopyrightBlock';
import useShallowEqualSelector from '../../hooks/useShallowEqualSelector';
import { actionUpdateAvatarRequest } from '../../store/actions/userAction';
import { FollowCard } from '../../components/FollowCard/FollowCard';
import { PostComponent } from '../../components/PostComponent/PostComponent';

export const UserProfile = () => {
  const { user } = useShallowEqualSelector((state) => state.user);
  const { posts, isLoading } = useShallowEqualSelector((state) => state.post);
  const userPosts = posts.filter((post) => post.username === user.username);
  const { followers } = useShallowEqualSelector((state) => state.followers);
  const { following } = useShallowEqualSelector((state) => state.following);
  const dispatch = useDispatch();
  const { register, handleSubmit } = useForm();
  const [state, setState] = useState({
    label: {
      title: 'Set profile photo',
    },
    submit: {
      classes: 'd-none',
    },
  });
  const followingCount = following ? following.length : 0;
  const followersCount = followers ? followers.length : 0;
  const avatar = `http://twitter-clone.loc:8091/${user.profileimg}`;

  const submitDispatch = (data, event) => {
    event.preventDefault();

    data.userId = user.id;

    dispatch(actionUpdateAvatarRequest(data));

    setState({
      label: {
        title: 'Set profile photo',
      },
      submit: {
        classes: 'd-none',
      },
    });
  };

  const fileInputHandler = () => {
    setState({
      label: {
        title: 'Photo selected, click Upload',
      },
      submit: {
        classes: 'upload-avatar__submit d-block mt-2',
      },
    });
  };

  return (
    <>
      <Header newClass="header" />

      <main className="UserProfile">
        <section className="container-fluid tabs">
          <div className="container">
            <div className="row">
              <div className="col-9 ml-auto tabs__wrapper">
                <Link to="/edit" className="tabs__edit ml-auto">
                  Edit Profile
                </Link>
              </div>
            </div>
          </div>
        </section>
        <div className="container">
          <div className="row pt-3">
            <section className="sidebar col-lg-3 p-md-0">
              {user.profileimg ? (
                <img src={avatar} alt="User avatar" className="user-photo" />
              ) : (
                <div className="user-photo" />
              )}

              <div className="card user-info">
                <div className="card-body d-flex flex-column">
                  <form className="upload-avatar" onSubmit={handleSubmit(submitDispatch)}>
                    <input
                      className="upload-avatar__input"
                      id="file-avatar"
                      type="file"
                      accept=".jpeg, .png, .gif, .jpg"
                      name="avatar"
                      ref={register}
                      onChange={fileInputHandler}
                    />
                    <label htmlFor="file-avatar" className="upload-avatar__label">
                      <span className="upload-avatar__label-icon-wrapper">
                        <img
                          className="input__file-icon"
                          src="../../public/img/icons/upload-icon.svg"
                          alt="Set profile avatar"
                          width="20"
                          height="20"
                        />
                      </span>
                      <span className="upload-avatar__label-button-text">{state.label.title}</span>
                    </label>

                    <input type="submit" className={state.submit.classes} value="Upload" />
                  </form>

                  <h4 className="card-title user-info__title mt-2">
                    {user.firstname} {user.lastname}
                  </h4>
                  <h6 className="card-subtitle mb-2 text-muted user-info__subtitle">
                    @{user.username}
                  </h6>
                  <p className="card-text user-info__text">
                    {user.bio ? user.bio : 'Edit your profile to add bio'}
                  </p>
                </div>
              </div>
            </section>
            <section className="main-column col-lg-6 p-md-0">
              <Tabs>
                <div className="tabs-wrapper">
                  <div className="col-9 tabs__group">
                    <TabLink to="tab1" activeClassName="tabs__info--active" className="tabs__info">
                      <span className="tabs__title">Posts</span>
                      <span className="tabs__quantity">
                        {user.postsCount ? user.postsCount : 0}
                      </span>
                    </TabLink>
                    <TabLink to="tab2" activeClassName="tabs__info--active" className="tabs__info">
                      <span className="tabs__title">Following</span>
                      <span className="tabs__quantity">{followingCount}</span>
                    </TabLink>

                    <TabLink to="tab3" activeClassName="tabs__info--active" className="tabs__info">
                      <span className="tabs__title">Followers</span>
                      <span className="tabs__quantity">{followersCount}</span>
                    </TabLink>
                  </div>

                  <TabContent for="tab1">
                    <section className="tweets-tab active-tab">
                      <PostForm userId={user.id} />
                      {!isLoading && userPosts.length > 0 ? (
                        userPosts.map((post) => (
                          <PostComponent
                            key={post.id}
                            id={post.id}
                            firstName={post.firstname}
                            lastName={post.lastname}
                            username={post.username}
                            likes={post.likes}
                            isLike={post.isLike}
                            text={post.body}
                            imgLink={post.post_img}
                            date={post.created_at}
                            avatarLink={post.profileimg}
                          />
                        ))
                      ) : (
                        <p className="no-tweets">No posts from you yet.</p>
                      )}
                    </section>
                  </TabContent>
                  <TabContent for="tab2">
                    <section className="following-tab">
                      {!following && (
                        <p className="no-following">You have not followed anybody yet.</p>
                      )}

                      <div className="following-tab-wrapper d-flex flex-wrap justify-content-between">
                        {following &&
                          following.map((user) => {
                            const inFollowing = following.filter((item) => item.id === user.id);
                            const isAlreadyFollowing = inFollowing.length > 0;

                            const inFollowers = followers
                              ? followers.filter((item) => item.id === user.id)
                              : [];
                            const isFollower =
                              inFollowers.length > 0 ? 'Follows you' : 'Not follows you';

                            return (
                              <FollowCard
                                key={user.id}
                                firstName={user.firstname}
                                lastName={user.lastname}
                                username={user.username}
                                avatarLink={user.profileimg}
                                userID={user.id}
                                isAlreadyFollowing={isAlreadyFollowing}
                                isFollower={isFollower}
                              />
                            );
                          })}
                      </div>
                    </section>
                  </TabContent>
                  <TabContent for="tab3">
                    <section className="followers-tab">
                      {!followers && <p className="no-followers">No followers yet.</p>}

                      <div className="following-tab-wrapper d-flex flex-wrap justify-content-between">
                        {followers &&
                          followers.map((user) => {
                            const results = following
                              ? following.filter((item) => item.id === user.id)
                              : [];

                            const isAlreadyFollowing = results.length > 0;

                            return (
                              <FollowCard
                                firstName={user.firstname}
                                lastName={user.lastname}
                                username={user.username}
                                avatarLink={user.profileimg}
                                userID={user.id}
                                isAlreadyFollowing={isAlreadyFollowing}
                              />
                            );
                          })}
                      </div>
                    </section>
                  </TabContent>
                </div>
              </Tabs>
            </section>
            <section className="col-md-3 right-sidebar">
              <RecommendsBlock />

              <CopyrightBlock />
            </section>
          </div>
        </div>
      </main>
    </>
  );
};
